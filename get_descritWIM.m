function [ regimPe ] = get_descritWIM( input_date, error )
%%Essa função é para o descritor no modo WIM

idx = [];
%error = 0.95;

for i=1:1:length(input_date)

    pos = i+4;
    
    if(pos > length(input_date))
      pos = length(input_date);
    end
    
    dateMaior = input_date(pos) + error;
    dateMenor = input_date(pos) - error;
  
    if input_date(i) <= dateMaior && input_date(i) >= dateMenor
    %if input_date(i) >= dateMenor
        idx = [idx; input_date(i)];
        %idx{i} = input_date(i);
    else 
        %idx{i} = 0;
        idx = [idx; 0];
    end
    
end

cont = 0;
aux = {};
vetor = [];
  
j = 1;
i = 1;

idx(end:end+4) = 0;

%for j=1:1:8
 for k = 1:1:length(idx)
        if idx(k) == 0
            cont = cont + 1;
            
            if cont > 4 
                if j ~= 1
                    aux{i} = vetor;
                    vetor = [];
                    i = i + 1;
                end
                
                j = 1;
                             
                continue;
            end
        else
           cont = 0;
           
           vetor(j) = idx(k);
           j = j + 1;
        end

      
 end
%end

%regimPe = [];
vetorAux = [];

for m = 1:1:i-1
   
    vetorAux = aux{1, m};
    
    if length(vetorAux) > 1
        regimPe(m) = complex(mean(aux{m}), length(aux{m})); 
        %regimPe(m) = mean(aux{m}); 
    else
        regimPe(m) = 0;
    end
    
end

regimPe = regimPe(find(regimPe~=0));


end

