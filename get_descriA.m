function [result, resultReduzida, resultData3, resultDataA4] = get_descriA(data, dataAA, dataAAA, dataA4)
               
        result1 = [complex(mean(data(1:46,1:1)), length(data(1:46,1:1)));
                   complex(mean(data(47:57,1:1)), length(data(47:57,1:1)));
                   complex(mean(data(58:118,1:1)), length(data(58:118,1:1)));
                   complex(mean(data(119:154,1:1)), length(data(119:154,1:1)));
                   complex(mean(data(155:242,1:1)), length(data(155:242,1:1)));
                   complex(mean(data(243:255,1:1)), length(data(243:255,1:1)));
                   complex(mean(data(256:330,1:1)), length(data(256:330,1:1)));
                   complex(mean(data(331:356,1:1)), length(data(331:356,1:1)));
                   complex(mean(data(357:448,1:1)), length(data(357:448,1:1)));
                   complex(mean(data(449:467,1:1)), length(data(449:467,1:1)));
                   complex(mean(data(468:546,1:1)), length(data(468:546,1:1)));
                   complex(mean(data(547:564,1:1)), length(data(547:564,1:1)));
                   complex(mean(data(565:661,1:1)), length(data(565:661,1:1)));
                   complex(mean(data(662:671,1:1)), length(data(662:671,1:1)));
                   complex(mean(data(672:727,1:1)), length(data(672:727,1:1)));
                   complex(mean(data(728:764,1:1)), length(data(728:764,1:1)));
                   complex(mean(data(765:845,1:1)), length(data(765:845,1:1)))];
               
               result2 = [complex(mean(data(1:46,2:2)), length(data(1:46,2:2)));
                   complex(mean(data(47:57,2:2)), length(data(47:57,2:2)));
                   complex(mean(data(58:138,2:2)), length(data(58:138,2:2)));
                   complex(mean(data(139:173,2:2)), length(data(139:173,2:2)));
                   complex(mean(data(174:242,2:2)), length(data(174:242,2:2)));
                   complex(mean(data(243:255,2:2)), length(data(243:255,2:2)));
                   complex(mean(data(256:330,2:2)), length(data(256:330,2:2)));
                   complex(mean(data(331:356,2:2)), length(data(331:356,2:2)));
                   complex(mean(data(357:448,2:2)), length(data(357:448,2:2)));
                   complex(mean(data(449:467,2:2)), length(data(449:467,2:2)));
                   complex(mean(data(468:546,2:2)), length(data(468:546,2:2)));
                   complex(mean(data(547:564,2:2)), length(data(547:564,2:2)));
                   complex(mean(data(565:661,2:2)), length(data(565:661,2:2)));
                   complex(mean(data(662:671,2:2)), length(data(662:671,2:2)));
                   complex(mean(data(672:727,2:2)), length(data(672:727,2:2)));
                   complex(mean(data(728:764,2:2)), length(data(728:764,2:2)));
                   complex(mean(data(765:845,2:2)), length(data(765:845,2:2)))];
               
         result3 = [complex(mean(data(1:46,3:3)), length(data(1:46,3:3)));
                   complex(mean(data(47:57,3:3)), length(data(47:57,3:3)));
                   complex(mean(data(58:138,3:3)), length(data(58:138,3:3)));
                   complex(mean(data(139:173,3:3)), length(data(139:173,3:3)));
                   complex(mean(data(174:242,3:3)), length(data(174:242,3:3)));
                   complex(mean(data(243:255,3:3)), length(data(243:255,3:3)));
                   complex(mean(data(256:330,3:3)), length(data(256:330,3:3)));
                   complex(mean(data(331:356,3:3)), length(data(331:356,3:3)));
                   complex(mean(data(357:448,3:3)), length(data(357:448,3:3)));
                   complex(mean(data(449:467,3:3)), length(data(449:467,3:3)));
                   complex(mean(data(468:546,3:3)), length(data(468:546,3:3)));
                   complex(mean(data(547:564,3:3)), length(data(547:564,3:3)));
                   complex(mean(data(565:661,3:3)), length(data(565:661,3:3)));
                   complex(mean(data(662:671,3:3)), length(data(662:671,3:3)));
                   complex(mean(data(672:727,3:3)), length(data(672:727,3:3)));
                   complex(mean(data(728:764,3:3)), length(data(728:764,3:3)));
                   complex(mean(data(765:845,3:3)), length(data(765:845,3:3)))];

         result4 = [complex(mean(data(1:46,4:4)), length(data(1:46,4:4)));
                   complex(mean(data(47:57,4:4)), length(data(47:57,4:4)));
                   complex(mean(data(58:138,4:4)), length(data(58:138,4:4)));
                   complex(mean(data(139:173,4:4)), length(data(139:173,4:4)));
                   complex(mean(data(174:242,4:4)), length(data(174:242,4:4)));
                   complex(mean(data(243:255,4:4)), length(data(243:255,4:4)));
                   complex(mean(data(256:310,4:4)), length(data(256:310,4:4)));
                   complex(mean(data(311:336,4:4)), length(data(311:336,4:4)));
                   complex(mean(data(337:448,4:4)), length(data(337:448,4:4)));
                   complex(mean(data(449:467,4:4)), length(data(449:467,4:4)));
                   complex(mean(data(468:546,4:4)), length(data(468:546,4:4)));
                   complex(mean(data(547:564,4:4)), length(data(547:564,4:4)));
                   complex(mean(data(565:661,4:4)), length(data(565:661,4:4)));
                   complex(mean(data(662:671,4:4)), length(data(662:671,4:4)));
                   complex(mean(data(672:697,4:4)), length(data(672:697,4:4)));
                   complex(mean(data(698:734,4:4)), length(data(698:734,4:4)));
                   complex(mean(data(735:845,4:4)), length(data(735:845,4:4)))];
                          
          result5 = [complex(mean(data(1:46,5:5)), length(data(1:46,5:5)));
                   complex(mean(data(47:57,5:5)), length(data(47:57,5:5)));
                   complex(mean(data(58:138,5:5)), length(data(58:138,5:5)));
                   complex(mean(data(139:173,5:5)), length(data(139:173,5:5)));
                   complex(mean(data(174:242,5:5)), length(data(174:242,5:5)));
                   complex(mean(data(243:255,5:5)), length(data(243:255,5:5)));
                   complex(mean(data(256:330,5:5)), length(data(256:330,5:5)));
                   complex(mean(data(331:356,5:5)), length(data(331:356,5:5)));
                   complex(mean(data(357:448,5:5)), length(data(357:448,5:5)));
                   complex(mean(data(449:467,5:5)), length(data(449:467,5:5)));
                   complex(mean(data(468:527,5:5)), length(data(468:527,5:5)));
                   complex(mean(data(528:545,5:5)), length(data(528:545,5:5)));
                   complex(mean(data(546:661,5:5)), length(data(546:661,5:5)));
                   complex(mean(data(662:671,5:5)), length(data(662:671,5:5)));
                   complex(mean(data(672:727,5:5)), length(data(672:727,5:5)));
                   complex(mean(data(728:764,5:5)), length(data(728:764,5:5)));
                   complex(mean(data(765:845,5:5)), length(data(765:845,5:5)))];
               
        result6 = [complex(mean(data(1:46,6:6)), length(data(1:46,6:6)));
                   complex(mean(data(47:57,6:6)), length(data(47:57,6:6)));
                   complex(mean(data(58:113,6:6)), length(data(58:113,6:6)));
                   complex(mean(data(114:148,6:6)), length(data(114:148,6:6)));
                   complex(mean(data(149:242,6:6)), length(data(149:242,6:6)));
                   complex(mean(data(243:255,6:6)), length(data(243:255,6:6)));
                   complex(mean(data(256:310,6:6)), length(data(256:310,6:6)));
                   complex(mean(data(311:336,6:6)), length(data(311:336,6:6)));
                   complex(mean(data(337:448,6:6)), length(data(337:448,6:6)));
                   complex(mean(data(449:467,6:6)), length(data(449:467,6:6)));
                   complex(mean(data(468:546,6:6)), length(data(468:546,6:6)));
                   complex(mean(data(547:564,6:6)), length(data(547:564,6:6)));
                   complex(mean(data(565:661,6:6)), length(data(565:661,6:6)));
                   complex(mean(data(662:671,6:6)), length(data(662:671,6:6)));
                   complex(mean(data(672:703,6:6)), length(data(672:703,6:6)));
                   complex(mean(data(704:739,6:6)), length(data(704:739,6:6)));
                   complex(mean(data(740:845,6:6)), length(data(740:845,6:6)))];
               
         result7 = [complex(mean(data(1:46,7:7)), length(data(1:46,7:7)));
                   complex(mean(data(47:57,7:7)), length(data(47:57,7:7)));
                   complex(mean(data(58:138,7:7)), length(data(58:138,7:7)));
                   complex(mean(data(139:173,7:7)), length(data(139:173,7:7)));
                   complex(mean(data(174:242,7:7)), length(data(174:242,7:7)));
                   complex(mean(data(243:255,7:7)), length(data(243:255,7:7)));
                   complex(mean(data(256:330,7:7)), length(data(256:330,7:7)));
                   complex(mean(data(331:356,7:7)), length(data(331:356,7:7)));
                   complex(mean(data(357:448,7:7)), length(data(357:448,7:7)));
                   complex(mean(data(449:467,7:7)), length(data(449:467,7:7)));
                   complex(mean(data(468:527,7:7)), length(data(468:527,7:7)));
                   complex(mean(data(528:545,7:7)), length(data(528:545,7:7)));
                   complex(mean(data(546:661,7:7)), length(data(546:661,7:7)));
                   complex(mean(data(662:671,7:7)), length(data(662:671,7:7)));
                   complex(mean(data(672:727,7:7)), length(data(672:727,7:7)));
                   complex(mean(data(728:764,7:7)), length(data(728:764,7:7)));
                   complex(mean(data(765:845,7:7)), length(data(765:845,7:7)))];
               
    result = [result1 result2 result3 result4 result5 result6 result7];
    
    %Fim dos sensorgramas com o tempo todo.
    
    %Inicio dos sensorgramas com o tempo 661 segundos
    
    result11 = [complex(mean(dataAA(1:46,1:1)), length(dataAA(1:46,1:1)));
                   complex(mean(dataAA(47:57,1:1)), length(dataAA(47:57,1:1)));
                   complex(mean(dataAA(58:138,1:1)), length(dataAA(58:138,1:1)));
                   complex(mean(dataAA(139:173,1:1)), length(dataAA(139:173,1:1)));
                   complex(mean(dataAA(174:242,1:1)), length(dataAA(174:242,1:1)));
                   complex(mean(dataAA(243:255,1:1)), length(dataAA(243:255,1:1)));
                   complex(mean(dataAA(256:330,1:1)), length(dataAA(256:330,1:1)));
                   complex(mean(dataAA(331:356,1:1)), length(dataAA(331:356,1:1)));
                   complex(mean(dataAA(357:448,1:1)), length(dataAA(357:448,1:1)));
                   complex(mean(dataAA(449:467,1:1)), length(dataAA(449:467,1:1)));
                   complex(mean(dataAA(468:546,1:1)), length(dataAA(468:546,1:1)));
                   complex(mean(dataAA(547:564,1:1)), length(dataAA(547:564,1:1)));
                   complex(mean(dataAA(565:661,1:1)), length(dataAA(565:661,1:1)))];
               
     result22 = [complex(mean(dataAA(1:46,2:2)), length(dataAA(1:46,2:2)));
                   complex(mean(dataAA(47:57,2:2)), length(dataAA(47:57,2:2)));
                   complex(mean(dataAA(58:138,2:2)), length(dataAA(58:138,2:2)));
                   complex(mean(dataAA(139:173,2:2)), length(dataAA(139:173,2:2)));
                   complex(mean(dataAA(174:242,2:2)), length(dataAA(174:242,2:2)));
                   complex(mean(dataAA(243:255,2:2)), length(dataAA(243:255,2:2)));
                   complex(mean(dataAA(256:330,2:2)), length(dataAA(256:330,2:2)));
                   complex(mean(dataAA(331:356,2:2)), length(dataAA(331:356,2:2)));
                   complex(mean(dataAA(357:448,2:2)), length(dataAA(357:448,2:2)));
                   complex(mean(dataAA(449:467,2:2)), length(dataAA(449:467,2:2)));
                   complex(mean(dataAA(468:546,2:2)), length(dataAA(468:546,2:2)));
                   complex(mean(dataAA(547:564,2:2)), length(dataAA(547:564,2:2)));
                   complex(mean(dataAA(565:661,2:2)), length(dataAA(565:661,2:2)))];
    
       result33 = [complex(mean(dataAA(1:46,3:3)), length(dataAA(1:46,3:3)));
                   complex(mean(dataAA(47:57,3:3)), length(dataAA(47:57,3:3)));
                   complex(mean(dataAA(58:138,3:3)), length(dataAA(58:138,3:3)));
                   complex(mean(dataAA(139:173,3:3)), length(dataAA(139:173,3:3)));
                   complex(mean(dataAA(174:242,3:3)), length(dataAA(174:242,3:3)));
                   complex(mean(dataAA(243:255,3:3)), length(dataAA(243:255,3:3)));
                   complex(mean(dataAA(256:330,3:3)), length(dataAA(256:330,3:3)));
                   complex(mean(dataAA(331:356,3:3)), length(dataAA(331:356,3:3)));
                   complex(mean(dataAA(357:448,3:3)), length(dataAA(357:448,3:3)));
                   complex(mean(dataAA(449:467,3:3)), length(dataAA(449:467,3:3)));
                   complex(mean(dataAA(468:546,3:3)), length(dataAA(468:546,3:3)));
                   complex(mean(dataAA(547:564,3:3)), length(dataAA(547:564,3:3)));
                   complex(mean(dataAA(565:661,3:3)), length(dataAA(565:661,3:3)))];
               
       result44 = [complex(mean(dataAA(1:46,4:4)), length(dataAA(1:46,4:4)));
                   complex(mean(dataAA(47:57,4:4)), length(dataAA(47:57,4:4)));
                   complex(mean(dataAA(58:123,4:4)), length(dataAA(58:123,4:4)));
                   complex(mean(dataAA(124:158,4:4)), length(dataAA(124:158,4:4)));
                   complex(mean(dataAA(159:241,4:4)), length(dataAA(159:241,4:4)));
                   complex(mean(dataAA(242:255,4:4)), length(dataAA(242:255,4:4)));
                   complex(mean(dataAA(256:330,4:4)), length(dataAA(256:330,4:4)));
                   complex(mean(dataAA(331:356,4:4)), length(dataAA(331:356,4:4)));
                   complex(mean(dataAA(357:448,4:4)), length(dataAA(357:448,4:4)));
                   complex(mean(dataAA(449:467,4:4)), length(dataAA(449:467,4:4)));
                   complex(mean(dataAA(468:526,4:4)), length(dataAA(468:526,4:4)));
                   complex(mean(dataAA(527:544,4:4)), length(dataAA(527:544,4:4)));
                   complex(mean(dataAA(545:661,4:4)), length(dataAA(545:661,4:4)))];
               
       result55 = [complex(mean(dataAA(1:46,5:5)), length(dataAA(1:46,5:5)));
                   complex(mean(dataAA(47:57,5:5)), length(dataAA(47:57,5:5)));
                   complex(mean(dataAA(58:138,5:5)), length(dataAA(58:138,5:5)));
                   complex(mean(dataAA(139:173,5:5)), length(dataAA(139:173,5:5)));
                   complex(mean(dataAA(174:242,5:5)), length(dataAA(174:242,5:5)));
                   complex(mean(dataAA(243:255,5:5)), length(dataAA(243:255,5:5)));
                   complex(mean(dataAA(256:312,5:5)), length(dataAA(256:312,5:5)));
                   complex(mean(dataAA(313:338,5:5)), length(dataAA(313:338,5:5)));
                   complex(mean(dataAA(339:448,5:5)), length(dataAA(339:448,5:5)));
                   complex(mean(dataAA(449:467,5:5)), length(dataAA(449:467,5:5)));
                   complex(mean(dataAA(468:546,5:5)), length(dataAA(468:546,5:5)));
                   complex(mean(dataAA(547:564,5:5)), length(dataAA(547:564,5:5)));
                   complex(mean(dataAA(565:661,5:5)), length(dataAA(565:661,5:5)))];
               
        result66 = [complex(mean(dataAA(1:46,6:6)), length(dataAA(1:46,6:6)));
                   complex(mean(dataAA(47:57,6:6)), length(dataAA(47:57,6:6)));
                   complex(mean(dataAA(58:138,6:6)), length(dataAA(58:138,6:6)));
                   complex(mean(dataAA(139:173,6:6)), length(dataAA(139:173,6:6)));
                   complex(mean(dataAA(174:242,6:6)), length(dataAA(174:242,6:6)));
                   complex(mean(dataAA(243:255,6:6)), length(dataAA(243:255,6:6)));
                   complex(mean(dataAA(256:303,6:6)), length(dataAA(256:303,6:6)));
                   complex(mean(dataAA(304:328,6:6)), length(dataAA(304:328,6:6)));
                   complex(mean(dataAA(329:448,6:6)), length(dataAA(329:448,6:6)));
                   complex(mean(dataAA(449:467,6:6)), length(dataAA(449:467,6:6)));
                   complex(mean(dataAA(468:537,6:6)), length(dataAA(468:537,6:6)));
                   complex(mean(dataAA(538:554,6:6)), length(dataAA(538:554,6:6)));
                   complex(mean(dataAA(555:661,6:6)), length(dataAA(555:661,6:6)))];
               
         result77 = [complex(mean(dataAA(1:46,7:7)), length(dataAA(1:46,7:7)));
                   complex(mean(dataAA(47:57,7:7)), length(dataAA(47:57,7:7)));
                   complex(mean(dataAA(58:138,7:7)), length(dataAA(58:138,7:7)));
                   complex(mean(dataAA(139:173,7:7)), length(dataAA(139:173,7:7)));
                   complex(mean(dataAA(174:242,7:7)), length(dataAA(174:242,7:7)));
                   complex(mean(dataAA(243:255,7:7)), length(dataAA(243:255,7:7)));
                   complex(mean(dataAA(256:303,7:7)), length(dataAA(256:303,7:7)));
                   complex(mean(dataAA(304:328,7:7)), length(dataAA(304:328,7:7)));
                   complex(mean(dataAA(329:448,7:7)), length(dataAA(329:448,7:7)));
                   complex(mean(dataAA(449:467,7:7)), length(dataAA(449:467,7:7)));
                   complex(mean(dataAA(468:537,7:7)), length(dataAA(468:537,7:7)));
                   complex(mean(dataAA(538:554,7:7)), length(dataAA(538:554,7:7)));
                   complex(mean(dataAA(555:661,7:7)), length(dataAA(555:661,7:7)))];
   
        resultReduzida = [result11 result22 result33 result44 result55 result66 result77];
        
        
      result111 = [complex(mean(dataAAA(1:46,1:1)), length(dataAAA(1:46,1:1)));
                   complex(mean(dataAAA(47:57,1:1)), length(dataAAA(47:57,1:1)));
                   complex(mean(dataAAA(58:138,1:1)), length(dataAAA(58:138,1:1)));
                   complex(mean(dataAAA(139:173,1:1)), length(dataAAA(139:173,1:1)));
                   complex(mean(dataAAA(174:242,1:1)), length(dataAAA(174:242,1:1)));
                   complex(mean(dataAAA(243:255,1:1)), length(dataAAA(243:255,1:1)));
                   complex(mean(dataAAA(256:330,1:1)), length(dataAAA(256:330,1:1)));
                   complex(mean(dataAAA(331:356,1:1)), length(dataAAA(331:356,1:1)));
                   complex(mean(dataAAA(357:449,1:1)), length(dataAAA(357:449,1:1)))];
               
      result222 = [complex(mean(dataAAA(1:46,2:2)), length(dataAAA(1:46,2:2)));
                   complex(mean(dataAAA(47:57,2:2)), length(dataAAA(47:57,2:2)));
                   complex(mean(dataAAA(58:138,2:2)), length(dataAAA(58:138,2:2)));
                   complex(mean(dataAAA(139:173,2:2)), length(dataAAA(139:173,2:2)));
                   complex(mean(dataAAA(174:242,2:2)), length(dataAAA(174:242,2:2)));
                   complex(mean(dataAAA(243:255,2:2)), length(dataAAA(243:255,2:2)));
                   complex(mean(dataAAA(256:330,2:2)), length(dataAAA(256:330,2:2)));
                   complex(mean(dataAAA(331:356,2:2)), length(dataAAA(331:356,2:2)));
                   complex(mean(dataAAA(357:449,2:2)), length(dataAAA(357:449,2:2)))];
               
      result333 = [complex(mean(dataAAA(1:46,3:3)), length(dataAAA(1:46,3:3)));
                   complex(mean(dataAAA(47:57,3:3)), length(dataAAA(47:57,3:3)));
                   complex(mean(dataAAA(58:138,3:3)), length(dataAAA(58:138,3:3)));
                   complex(mean(dataAAA(139:173,3:3)), length(dataAAA(139:173,3:3)));
                   complex(mean(dataAAA(174:242,3:3)), length(dataAAA(174:242,3:3)));
                   complex(mean(dataAAA(243:255,3:3)), length(dataAAA(243:255,3:3)));
                   complex(mean(dataAAA(256:330,3:3)), length(dataAAA(256:330,3:3)));
                   complex(mean(dataAAA(331:356,3:3)), length(dataAAA(331:356,3:3)));
                   complex(mean(dataAAA(357:449,3:3)), length(dataAAA(357:449,3:3)))];
               
      result444 = [complex(mean(dataAAA(1:46,4:4)), length(dataAAA(1:46,4:4)));
                   complex(mean(dataAAA(47:57,4:4)), length(dataAAA(47:57,4:4)));
                   complex(mean(dataAAA(58:125,4:4)), length(dataAAA(58:125,4:4)));
                   complex(mean(dataAAA(126:160,4:4)), length(dataAAA(126:160,4:4)));
                   complex(mean(dataAAA(161:242,4:4)), length(dataAAA(161:242,4:4)));
                   complex(mean(dataAAA(243:255,4:4)), length(dataAAA(243:255,4:4)));
                   complex(mean(dataAAA(256:330,4:4)), length(dataAAA(256:330,4:4)));
                   complex(mean(dataAAA(331:356,4:4)), length(dataAAA(331:356,4:4)));
                   complex(mean(dataAAA(357:449,4:4)), length(dataAAA(357:449,4:4)))];
              
       result555 = [complex(mean(dataAAA(1:46,5:5)), length(dataAAA(1:46,5:5)));
                   complex(mean(dataAAA(47:57,5:5)), length(dataAAA(47:57,5:5)));
                   complex(mean(dataAAA(58:138,5:5)), length(dataAAA(58:138,5:5)));
                   complex(mean(dataAAA(139:173,5:5)), length(dataAAA(139:173,5:5)));
                   complex(mean(dataAAA(174:242,5:5)), length(dataAAA(174:242,5:5)));
                   complex(mean(dataAAA(243:255,5:5)), length(dataAAA(243:255,5:5)));
                   complex(mean(dataAAA(256:312,5:5)), length(dataAAA(256:312,5:5)));
                   complex(mean(dataAAA(313:335,5:5)), length(dataAAA(313:335,5:5)));
                   complex(mean(dataAAA(336:449,5:5)), length(dataAAA(336:449,5:5)))];
               
       result667 = [complex(mean(dataAAA(1:46,6:6)), length(dataAAA(1:46,6:6)));
                   complex(mean(dataAAA(47:57,6:6)), length(dataAAA(47:57,6:6)));
                   complex(mean(dataAAA(58:115,6:6)), length(dataAAA(58:115,6:6)));
                   complex(mean(dataAAA(116:151,6:6)), length(dataAAA(116:151,6:6)));
                   complex(mean(dataAAA(152:242,6:6)), length(dataAAA(152:242,6:6)));
                   complex(mean(dataAAA(243:255,6:6)), length(dataAAA(243:255,6:6)));
                   complex(mean(dataAAA(256:330,6:6)), length(dataAAA(256:322,6:6)));
                   complex(mean(dataAAA(331:356,6:6)), length(dataAAA(323:349,6:6)));
                   complex(mean(dataAAA(357:449,6:6)), length(dataAAA(350:449,6:6)))];
     
      result777 = [complex(mean(dataAAA(1:46,7:7)), length(dataAAA(1:46,7:7)));
                   complex(mean(dataAAA(47:57,7:7)), length(dataAAA(47:57,7:7)));
                   complex(mean(dataAAA(58:138,7:7)), length(dataAAA(58:138,7:7)));
                   complex(mean(dataAAA(139:173,7:7)), length(dataAAA(139:173,7:7)));
                   complex(mean(dataAAA(174:242,7:7)), length(dataAAA(174:242,7:7)));
                   complex(mean(dataAAA(243:255,7:7)), length(dataAAA(243:255,7:7)));
                   complex(mean(dataAAA(256:330,7:7)), length(dataAAA(256:330,7:7)));
                   complex(mean(dataAAA(331:356,7:7)), length(dataAAA(331:356,7:7)));
                   complex(mean(dataAAA(357:449,7:7)), length(dataAAA(357:449,7:7)))];
               
               
     resultData3 = [result111 result222 result333 result444 result555 result667 result777];
     
     
     resultA41 = [complex(mean(dataA4(1:46,1:1)), length(dataA4(1:46,1:1)));
                   complex(mean(dataA4(47:57,1:1)), length(dataA4(47:57,1:1)));
                   complex(mean(dataA4(58:138,1:1)), length(dataA4(58:138,1:1)));
                   complex(mean(dataA4(139:173,1:1)), length(dataA4(139:173,1:1)));
                   complex(mean(dataA4(174:241,1:1)), length(dataA4(174:241,1:1)))];
               
     resultA42 = [complex(mean(dataA4(1:46,2:2)), length(dataA4(1:46,2:2)));
                   complex(mean(dataA4(47:57,2:2)), length(dataA4(47:57,2:2)));
                   complex(mean(dataA4(58:138,2:2)), length(dataA4(58:138,2:2)));
                   complex(mean(dataA4(139:173,2:2)), length(dataA4(139:173,2:2)));
                   complex(mean(dataA4(174:241,2:2)), length(dataA4(174:241,2:2)))];
               
     resultA43 = [complex(mean(dataA4(1:46,3:3)), length(dataA4(1:46,3:3)));
                   complex(mean(dataA4(47:57,3:3)), length(dataA4(47:57,3:3)));
                   complex(mean(dataA4(58:138,3:3)), length(dataA4(58:138,3:3)));
                   complex(mean(dataA4(139:173,3:3)), length(dataA4(139:173,3:3)));
                   complex(mean(dataA4(174:241,3:3)), length(dataA4(174:241,3:3)))];
               
     resultA44 = [complex(mean(dataA4(1:46,4:4)), length(dataA4(1:46,4:4)));
                   complex(mean(dataA4(47:57,4:4)), length(dataA4(47:57,4:4)));
                   complex(mean(dataA4(58:108,4:4)), length(dataA4(58:108,4:4)));
                   complex(mean(dataA4(109:143,4:4)), length(dataA4(109:143,4:4)));
                   complex(mean(dataA4(144:241,4:4)), length(dataA4(144:241,4:4)))];
               
     resultA45 = [complex(mean(dataA4(1:46,5:5)), length(dataA4(1:46,5:5)));
                   complex(mean(dataA4(47:57,5:5)), length(dataA4(47:57,5:5)));
                   complex(mean(dataA4(58:138,5:5)), length(dataA4(58:138,5:5)));
                   complex(mean(dataA4(139:173,5:5)), length(dataA4(139:173,5:5)));
                   complex(mean(dataA4(174:241,5:5)), length(dataA4(174:241,5:5)))];
     
    resultDataA4 = [resultA41 resultA42 resultA43 resultA44 resultA45];
     
               
end