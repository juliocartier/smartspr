function varargout = classificationGui(varargin)
% CLASSIFICATIONGUI MATLAB code for classificationGui.fig
%      CLASSIFICATIONGUI, by itself, creates a new CLASSIFICATIONGUI or raises the existing
%      singleton*.
%
%      H = CLASSIFICATIONGUI returns the handle to a new CLASSIFICATIONGUI or the handle to
%      the existing singleton*.
%
%      CLASSIFICATIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLASSIFICATIONGUI.M with the given input arguments.
%
%      CLASSIFICATIONGUI('Property','Value',...) creates a new CLASSIFICATIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before classificationGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to classificationGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help classificationGui

% Last Modified by GUIDE v2.5 07-Oct-2019 14:50:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @classificationGui_OpeningFcn, ...
                   'gui_OutputFcn',  @classificationGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before classificationGui is made visible.
function classificationGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to classificationGui (see VARARGIN)

% Choose default command line output for classificationGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes classificationGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = classificationGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in open.
function open_Callback(hObject, eventdata, handles)
% hObject    handle to open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guidata(gcbf,handles);


%guidata(gcbf,handles);
[file path] = uigetfile({'*.txt'}, 'File Selector');

ll = load(fullfile(path,file));
% assignin('base','BK7Ag9',BK7Ag9)

%Leitura dos arquivos no Windows
% dir_to_search = 'C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\';

%Leitura dos arquivos no Linux
%txtpattern = fullfile('/home/julio/GUI MATLAB/SmartSPR/ReadWIM/', '*.txt');
txtpattern = fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', '*.txt');
dinfo = dir(txtpattern);

%%Leitura dos arquivos em cada sensorgrama
BK7Ag9 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(1).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
PCAu9 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(2).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
BK7Au3 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(3).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
BK7Au5 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(4).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
BK7Au7 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(5).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
BK7Au9 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(6).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
PMMAAu9 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(7).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
TOPASAg9 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\', dinfo(8).name));  %Ler o diretorio e como tambem o arquivo que esta na pasta
%%Fim da leitura dos arquivos

if (length(ll) == length(BK7Ag9))
    result_descritBK7Au = get_descritWIM(ll, 1.6);  
    dataName = 'BK7Ag';
    
    result_sizeMonoSPR = result_descritBK7Au;
   
    qtyCoefficient = length(result_descritBK7Au);
    
    get_plotDescrit = get_dataClassificy(qtyCoefficient);
    
elseif (length(ll) == length(PCAu9))
     disp('PCAu9');
elseif (length(ll) == length(BK7Au9))
   %ll � o arquivo carregado para reconhecer em qual conjunto de dados
   %vai ser carregado.
   result_descritBK7Au = get_descritWIM(ll, 0.95);  
   dataName = 'BK7Au';
   
   result_sizeMonoSPR = result_descritBK7Au;
   
   qtyCoefficient = length(result_descritBK7Au);
   
   txtpattern = fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', '*.mat');
   dinfo = dir(txtpattern);
       
    Regim9A1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(1).name));  %Ler o diretorio e como tambem o arquivo que está na pasta
    Regim9B1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(5).name));  %Ler o diretorio e como tambem o arquivo que está na pasta
    Regim9C1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(9).name));  %Ler o diretorio e como tambem o arquivo que está na pasta
    Regim9D1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(12).name));  %Ler o diretorio e como tambem o arquivo que está na pasta

    resultA1 = get_VerifySize(Regim9A1.resToTA1, length(Regim9A1.resToTA1(:,1:1)), 9);
    resultB1 = get_VerifySize(Regim9B1.resToTB1, length(Regim9B1.resToTB1(:,1:1)), 9);
    resultC1 = get_VerifySize(Regim9C1.resToTC1, length(Regim9C1.resToTC1(:,1:1)), 9);
    resultD1 = get_VerifySize(Regim9D1.resToTD1, length(Regim9D1.resToTD1(:,1:1)), 9);

    get_plotDescrit = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))'; 
                       resultC1 repelem(9,length(resultC1(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))'];

 elseif (length(ll) == length(BK7Au7)) % Aqui comeca a parte em que ler
                                       % os dados de 7 regimes permanentes
                                       % para classificar
  
      result_descritBK7Au = get_descritWIM(ll, 0.95); %Essa funcao faz para encontrar
                                                   %os regimes permanentes
                                                   %de acordo com um erro
      result_sizeMonoSPR = result_descritBK7Au; %Armazena em uma variavel
      dataName = 'BK7Au';
      
      qtyCoefficient = length(result_descritBK7Au);
      
      %A variavel txtpattern � uma variavel em que � um caminho para a
      %pasta ReadBK7AuWIM
      txtpattern = fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', '*.mat');
      dinfo = dir(txtpattern); % Ler o diretorio
    
        %Ler os arquivos .mat em que estao na pasta ReadBK7AuWIM
        Regim9A1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(1).name));
        Regim9A2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(2).name));
        Regim9B1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(5).name));
        Regim9B2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(6).name));
        Regim9C1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(9).name));
        Regim9C2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(10).name));  
        Regim9D1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(12).name));
        Regim9D2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(13).name));
        
        %A funcao get_VerifySize, verifica o tamanho do conjunto de dados
        %de treinamento e converte e armazena em uma variavel
        resultA1 = get_VerifySize(Regim9A1.resToTA1, 7, 7);
        resultA2 = get_VerifySize(Regim9A2.resToTA2, 7, 7);
        resultB1 = get_VerifySize(Regim9B1.resToTB1, 7, 7);
        resultB2 = get_VerifySize(Regim9B2.resToTB2, 7, 7);
        resultC1 = get_VerifySize(Regim9C1.resToTC1, 7, 7);
        resultC2 = get_VerifySize(Regim9C2.resToTC2, 7, 7);
        resultD1 = get_VerifySize(Regim9D1.resToTD1, 7, 7);
        resultD2 = get_VerifySize(Regim9D2.resToTD2, 7, 7);
  %E armazenado em um vetor todas as variaveis em que representa o conjunto 
  %de dados de treinamento.
  get_plotDescrit = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))'];

 elseif (length(ll) == length(BK7Au5)) % Aqui comeca a parte em que ler
                                       % os dados de 5 regimes permanentes
                                       % para classificar
  
 result_descritBK7Au = get_descritWIM(ll, 0.95); %Essa funcao faz para encontrar
                                                   %os regimes permanentes
                                                   %de acordo com um erro
      result_sizeMonoSPR = result_descritBK7Au; %Armazena em uma variavel
      dataName = 'BK7Au';
      
      assignin('base','result_descritBK7Au', result_descritBK7Au)
      
      qtyCoefficient = length(result_descritBK7Au);
      
      %A variavel txtpattern � uma variavel em que � um caminho para a
      %pasta ReadBK7AuWIM
      txtpattern = fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', '*.mat');
      dinfo = dir(txtpattern); % Ler o diretorio
        
      
        %Ler os arquivos .mat em que estao na pasta ReadBK7AuWIM com 5
        %regimes
        Regim9A1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(1).name));
        Regim9A2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(2).name));
        Regim9A3 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(3).name));
        Regim9B1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(5).name));
        Regim9B2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(6).name));
        Regim9B3 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(7).name));
        Regim9C1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(9).name));
        Regim9C2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(10).name));  
        Regim9C3 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(11).name));  
        Regim9D1 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(12).name));
        Regim9D2 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(13).name));
        Regim9D3 = load(fullfile('C:\Users\LEIVA_UFERSA\SmartSPR\smartspr\ReadWIM\ReadBK7AuWIM\', dinfo(14).name));
  
        %A funcao get_VerifySize, verifica o tamanho do conjunto de dados
        %de treinamento e converte e armazena em uma variavel
        resultA1 = get_VerifySize(Regim9A1.resToTA1, 5, 5);
        resultA2 = get_VerifySize(Regim9A2.resToTA2, 5, 5);
        resultA3 = get_VerifySize(Regim9A3.resToTA3, 5, 5);
        resultB1 = get_VerifySize(Regim9B1.resToTB1, 5, 5);
        resultB2 = get_VerifySize(Regim9B2.resToTB2, 5, 5);
        resultB3 = get_VerifySize(Regim9B3.resToTB3, 5, 5);
        resultC1 = get_VerifySize(Regim9C1.resToTC1, 5, 5);
        resultC2 = get_VerifySize(Regim9C2.resToTC2, 5, 5);
        resultC3 = get_VerifySize(Regim9C3.resToTC3, 5, 5);
        resultD1 = get_VerifySize(Regim9D1.resToTD1, 5, 5);
        resultD2 = get_VerifySize(Regim9D2.resToTD2, 5, 5);
        resultD3 = get_VerifySize(Regim9D3.resToTD3, 5, 5);
        
 get_plotDescrit = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))';
                     resultB2 repelem(6,length(resultB2(:,1:1)))'; resultB3 repelem(7,length(resultB3(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultC3 repelem(11,length(resultC3(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))';
                     resultD2 repelem(13,length(resultD2(:,1:1)))'; resultD3 repelem(14,length(resultD3(:,1:1)))'];
                 
 elseif (length(ll) == length(PMMAAu9))
    disp('PMMAAu9');
 elseif (length(ll) == length(TOPASAg9))
    disp('TOPASAg9');
end

% assignin('base','BK7Ag9', ll)
% assignin('base','PCAu9', PCAu9)
% assignin('base','BK7Au9', BK7Au9)
% assignin('base','PMMAAu9', PMMAAu9)
% assignin('base','TOPASAg9', TOPASAg9)

resultQtyTable = [real(result_sizeMonoSPR)' imag(result_sizeMonoSPR)']; 
%A resultQtyTable Armazena os valores do sensorgrama em que passou os 
%dados de entrados do sensorgrama e envia para uma tabela na interface 
%grafica.

%armazena os dados do vetor para apresentar na interface grafica
set(handles.TblResultDescrit, 'data', resultQtyTable);
   
%Exibe os dados em que foi lido no grafico
 axes(handles.chartWIM1);
 plot(ll);
 xlabel('Time (s)');
 ylabel('\lambda_r (nm)');

%Grafico para apresentar os coeficientes do descritor 
axes(handles.chartWIM4);
plot3(get_plotDescrit(:,1:1), get_plotDescrit(:,2:2), get_plotDescrit(:,3:3), '.r', 'MarkerSize', 20);
hold on;
plot3(real(result_sizeMonoSPR(:,1:1)), real(result_sizeMonoSPR(:,2:2)), real(result_sizeMonoSPR(:,3:3)), '.b', 'MarkerSize', 20);
xlabel('1� Coefficient');
ylabel('2� Coefficient');
zlabel('3� Coefficient'); 

 dataSensorgram = (ll(:,1:1));
 handles.data = dataSensorgram; %Armazena os dados do sensorgrama que foi lido
 handles.qtyCoef = qtyCoefficient; %Armazena a quantidade de coeficientes
 handles.resultTest = result_sizeMonoSPR; %Armazena os valores para o teste
 handles.resultTrain = get_plotDescrit; %Armazena os valores para o treino
 handles.nameRecipe = dataName; %Armazena o nome em qual base de dados foi lido
 guidata(handles.calc, handles); %Realiza os calculos para funcao mais embaixo



% --- Executes on button press in gaussian.
function gaussian_Callback(hObject, eventdata, handles)
% string1 = get(handles.gaussian, 'Tag');
% handles.string1= string1;
% guidata(handles.calc, handles);

% --- Executes on button press in axisY.
function axisY_Callback(hObject, eventdata, handles)
% string1 = get(handles.axisY, 'Tag');
% handles.string1= string1;
% guidata(handles.calc, handles);

% --- Executes on button press in temporalC.
function temporalC_Callback(hObject, eventdata, handles)
% string1 = get(handles.temporalC, 'Tag');
% handles.string1= string1;
% guidata(handles.calc, handles);


function valueCalc_Callback(hObject, eventdata, handles)
% hObject    handle to valueCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of valueCalc as text
%        str2double(get(hObject,'String')) returns contents of valueCalc as a double


% --- Executes during object creation, after setting all properties.
function valueCalc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to valueCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in calc.
function calc_Callback(hObject, eventdata, handles)
% hObject    handle to calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

valueCalc = str2num(get(findobj(gcf, 'Tag', 'valueCalc'), 'String'));
guidata(gcbf,handles);

%Ler as variaveis
valueGaussian = get(handles.gaussian, 'Value');
valueTemporalC = get(handles.temporalC, 'Value');
valueAxisY = get(handles.axisY, 'Value');
data_ll = handles.data;
result_test = handles.resultTest; %Valores para o teste
result_train = handles.resultTrain;
name = handles.nameRecipe;


qtd_coeff = handles.qtyCoef; %Quantidade de Coeficientes do descritor

if (valueGaussian == 1 && valueTemporalC == 0 && valueAxisY == 0)
    result = get_noiseGaussian(data_ll, valueCalc*100);
    
    %Codigo da Regressão Linear 
    [IC_A, IC_B, coef_A] = get_resultRegress(5, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
    
    z = ones(length(result),1)*result(1);
    
       %Grafico no Modo WIM das regiões dos regimes permanentes
        axes(handles.chartWIM2);
        plot(result);
        hold on;
        plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
                
        %Grafico no Modo WIM das regiões de transição
        axes(handles.chartWIM3);
        plot(result);
        hold on;
        plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
        
        %Grafico no Modo WIM para os intervalos de confianças
        figure(1)
        plot(IC_A+result(1), 'b');
        hold on;
        plot(IC_B+result(1), 'r');
        hold on;
        plot(coef_A+result(1), 'k');
        hold on;
        plot([1:length(result)]', z, 'g');

elseif (valueGaussian == 0 && valueTemporalC == 0 && valueAxisY == 1)
    result = get_axisY(data_ll, valueCalc);
    
    %Codigo da Regressão Linear 
    [IC_A, IC_B, coef_A] = get_resultRegress(5, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
    
    z = ones(length(result),1)*result(1);
    
        %Grafico no Modo WIM das regioes dos regimes permanentes
        axes(handles.chartWIM2);
        plot(result);
        hold on;
        plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
                
        %Grafico no Modo WIM das regioes de transição
        axes(handles.chartWIM3);
        plot(result);
        hold on;
        plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
        
        %Grafico no Modo WIM para os intervalos de confianças

        figure(1)
        plot(IC_A+result(1), 'b');
        hold on;
        plot(IC_B+result(1), 'r');
        hold on;
        plot(coef_A+result(1), 'k');
        hold on;
        plot([1:length(result)]', z, 'g');
      
elseif (valueGaussian == 0 && valueTemporalC == 1 && valueAxisY == 0)
      
    result = get_region(data_ll, valueCalc(1), valueCalc(2), valueCalc(3));
    
    [IC_A, IC_B, coef_A] = get_resultRegress(5, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
    
    z = ones(length(result),1)*result(1);
    
        %Grafico no Modo WIM das regioes dos regimes permanentes
        axes(handles.chartWIM2);
        plot(result);
        hold on;
        plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
                
        %Grafico no Modo WIM das regioes de transição
        axes(handles.chartWIM3);
        plot(result);
        hold on;
        plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\lambda_r (nm)');
        
        %Grafico no Modo WIM para os intervalos de confianca        
        figure(1)
        plot(real(IC_A)+result(1), 'b');
        hold on;
        plot(real(IC_B)+result(1), 'r');
        hold on;
        plot(coef_A+result(1), 'k');
        hold on;
        plot([1:length(result)]', z, 'g');
    
elseif (valueGaussian == 0 && valueTemporalC == 0 && valueAxisY == 0)
    % O primeiro parametro da funcao get_resultRegress a janela da regressao
    % O Segundo valor sao os dados do sensorgrama
    
    result = data_ll;
    
    [IC_A, IC_B, coef_A] = get_resultRegress(5, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
    
    z = ones(length(result),1)*result(1);
    
    axes(handles.chartWIM2);
    plot(result);
    hold on;
    plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
    xlabel('Tempo (s)');
    ylabel('\lambda_r (nm)');
    
    %Grafico no Modo WIM das regimes de transicao
    axes(handles.chartWIM3);
    plot(result);
    hold on;
    plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
    xlabel('Tempo (s)');
    ylabel('\lambda_r (nm)');
    
    figure(1);
%     plot(IC_A+result(1), 'b');
%     hold on;
%     plot(IC_B+result(1), 'r');
%     hold on;
    plot(coef_A+result(1), 'k');
    hold on;
    plot([1:length(result)]', z, 'g');
       
end

%A variavel mdl faz o treino com o algoritmo knn.
mdl = fitcknn(result_train(:,1:end-1), result_train(:,end:end), 'NumNeighbors', 3, 'Distance' , 'euclidean');
%A variavel res ela faz a previsao com o modelo de treino e dar o resultado
res = predict(mdl, real(result_test(:,1:qtd_coeff)))

resultData = get_response(name, res); %Apresenta o resultado em qual
                                      %conjunto de dados vai pertecer
 
set(handles.resultString, 'String', resultData); %Apresenta o valor na GUI
 
set(handles.qtdRP, 'String', qtd_coeff); %Apresenta a quantidade de RP na GUI

function resultQty_Callback(hObject, eventdata, handles)
% hObject    handle to resultQty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resultQty as text
%        str2double(get(hObject,'String')) returns contents of resultQty as a double


% --- Executes during object creation, after setting all properties.
function resultQty_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resultQty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function resultString_Callback(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resultString as text
%        str2double(get(hObject,'String')) returns contents of resultString as a double


% --- Executes during object creation, after setting all properties.
function resultString_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function chartWIM1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chartWIM1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate chartWIM1
xlabel('Time (s)');
ylabel('\lambda_r (nm)');


% --- Executes during object creation, after setting all properties.
function text11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
%set(handles.text11, 'lambda');
%set(findobj(gcf, 'Tag', 'text11'), 'String', '\lambda');
%set(findobj(gca,'Tag','text11'), 'String', '\lambda');


% --- Executes during object creation, after setting all properties.
function chartWIM2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chartWIM2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate chartWIM2
xlabel('Time (s)');
ylabel('\lambda_r (nm)');


% --- Executes during object creation, after setting all properties.
function chartWIM3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chartWIM3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate chartWIM3
xlabel('Time (s)');
ylabel('\lambda_r (nm)');


% --- Executes on button press in aim.
function aim_Callback(hObject, eventdata, handles)
% hObject    handle to aim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
modo = get(handles.aim,'Tag')
switch modo
    case 'aim'
        close classificationGui;
        run ('classificationGuiAIM');
end

% --- Executes on button press in wim.
function wim_Callback(hObject, eventdata, handles)
% hObject    handle to wim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
modo = get(handles.wim, 'Tag')
switch modo
    case 'wim'
        close classificationGuiAIM;
        run ('classificationGui');
end



function resultData_Callback(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resultString as text
%        str2double(get(hObject,'String')) returns contents of resultString as a double


% --- Executes during object creation, after setting all properties.
function resultData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function chartWIM4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chartWIM4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate chartWIM4
xlabel('1� Coefficient');
ylabel('2� Coefficient');
zlabel('3� Coefficient');
