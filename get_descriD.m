function [result, resultData, resultData2, resultDataD4] = get_descriD(data, dataDD, dataDDD, dataD4)
        
    result1 = [complex(mean(data(1:46,1:1)), length(data(1:46,1:1)));
                   complex(mean(data(47:64,1:1)), length(data(47:64,1:1)));
                   complex(mean(data(65:142,1:1)), length(data(65:142,1:1)));
                   complex(mean(data(143:153,1:1)), length(data(143:153,1:1)));
                   complex(mean(data(154:258,1:1)), length(data(154:258,1:1)));
                   complex(mean(data(259:267,1:1)), length(data(259:267,1:1)));
                   complex(mean(data(268:309,1:1)), length(data(268:309,1:1)));
                   complex(mean(data(310:344,1:1)), length(data(310:344,1:1)));
                   complex(mean(data(345:442,1:1)), length(data(345:442,1:1)));
                   complex(mean(data(443:451,1:1)), length(data(443:451,1:1)));
                   complex(mean(data(452:534,1:1)), length(data(452:534,1:1)));
                   complex(mean(data(535:559,1:1)), length(data(535:559,1:1)));
                   complex(mean(data(560:637,1:1)), length(data(560:637,1:1)));
                   complex(mean(data(638:650,1:1)), length(data(638:650,1:1)));
                   complex(mean(data(651:726,1:1)), length(data(651:726,1:1)));
                   complex(mean(data(727:746,1:1)), length(data(727:746,1:1)));
                   complex(mean(data(747:845,1:1)), length(data(747:845,1:1)))];
    
           
        result2 = [complex(mean(data(1:46,2:2)), length(data(1:46,2:2)));
                   complex(mean(data(47:64,2:2)), length(data(47:64,2:2)));
                   complex(mean(data(65:142,2:2)), length(data(65:142,2:2)));
                   complex(mean(data(143:153,2:2)), length(data(143:153,2:2)));
                   complex(mean(data(154:258,2:2)), length(data(154:258,2:2)));
                   complex(mean(data(259:267,2:2)), length(data(259:267,2:2)));
                   complex(mean(data(268:324,2:2)), length(data(268:324,2:2)));
                   complex(mean(data(325:356,2:2)), length(data(325:356,2:2)));
                   complex(mean(data(357:442,2:2)), length(data(357:442,2:2)));
                   complex(mean(data(443:451,2:2)), length(data(443:451,2:2)));
                   complex(mean(data(452:534,2:2)), length(data(452:534,2:2)));
                   complex(mean(data(535:572,2:2)), length(data(535:572,2:2)));
                   complex(mean(data(573:637,2:2)), length(data(573:637,2:2)));
                   complex(mean(data(638:650,2:2)), length(data(638:650,2:2)));
                   complex(mean(data(651:726,2:2)), length(data(651:726,2:2)));
                   complex(mean(data(727:746,2:2)), length(data(727:746,2:2)));
                   complex(mean(data(747:845,2:2)), length(data(747:845,2:2)))];
               
         result3 = [complex(mean(data(1:46,3:3)), length(data(1:46,3:3)));
                   complex(mean(data(47:64,3:3)), length(data(47:64,3:3)));
                   complex(mean(data(65:142,3:3)), length(data(65:142,3:3)));
                   complex(mean(data(143:153,3:3)), length(data(143:153,3:3)));
                   complex(mean(data(154:258,3:3)), length(data(154:258,3:3)));
                   complex(mean(data(259:267,3:3)), length(data(259:267,3:3)));
                   complex(mean(data(268:324,3:3)), length(data(268:324,3:3)));
                   complex(mean(data(325:356,3:3)), length(data(325:356,3:3)));
                   complex(mean(data(357:442,3:3)), length(data(357:442,3:3)));
                   complex(mean(data(443:451,3:3)), length(data(443:451,3:3)));
                   complex(mean(data(452:534,3:3)), length(data(452:534,3:3)));
                   complex(mean(data(535:572,3:3)), length(data(535:572,3:3)));
                   complex(mean(data(573:637,3:3)), length(data(573:637,3:3)));
                   complex(mean(data(638:650,3:3)), length(data(638:650,3:3)));
                   complex(mean(data(651:726,3:3)), length(data(651:726,3:3)));
                   complex(mean(data(727:746,3:3)), length(data(727:746,3:3)));
                   complex(mean(data(747:845,3:3)), length(data(747:845,3:3)))];
               
        result4 = [complex(mean(data(1:46,4:4)), length(data(1:46,4:4)));
                   complex(mean(data(47:64,4:4)), length(data(47:64,4:4)));
                   complex(mean(data(65:122,4:4)), length(data(65:122,4:4)));
                   complex(mean(data(123:137,4:4)), length(data(123:137,4:4)));
                   complex(mean(data(138:258,4:4)), length(data(138:258,4:4)));
                   complex(mean(data(259:267,4:4)), length(data(259:267,4:4)));
                   complex(mean(data(268:324,4:4)), length(data(268:324,4:4)));
                   complex(mean(data(325:356,4:4)), length(data(325:356,4:4)));
                   complex(mean(data(357:442,4:4)), length(data(357:442,4:4)));
                   complex(mean(data(443:451,4:4)), length(data(443:451,4:4)));
                   complex(mean(data(452:534,4:4)), length(data(452:534,4:4)));
                   complex(mean(data(535:572,4:4)), length(data(535:572,4:4)));
                   complex(mean(data(573:637,4:4)), length(data(573:637,4:4)));
                   complex(mean(data(638:650,4:4)), length(data(638:650,4:4)));
                   complex(mean(data(651:711,4:4)), length(data(651:711,4:4)));
                   complex(mean(data(712:740,4:4)), length(data(712:740,4:4)));
                   complex(mean(data(741:845,4:4)), length(data(741:845,4:4)))];
   
        result5 = [complex(mean(data(1:46,5:5)), length(data(1:46,5:5)));
                   complex(mean(data(47:64,5:5)), length(data(47:64,5:5)));
                   complex(mean(data(65:142,5:5)), length(data(65:142,5:5)));
                   complex(mean(data(143:153,5:5)), length(data(143:153,5:5)));
                   complex(mean(data(154:258,5:5)), length(data(154:258,5:5)));
                   complex(mean(data(259:267,5:5)), length(data(259:267,5:5)));
                   complex(mean(data(268:324,5:5)), length(data(268:324,5:5)));
                   complex(mean(data(325:356,5:5)), length(data(325:356,5:5)));
                   complex(mean(data(357:442,5:5)), length(data(357:442,5:5)));
                   complex(mean(data(443:451,5:5)), length(data(443:451,5:5)));
                   complex(mean(data(452:522,5:5)), length(data(452:522,5:5)));
                   complex(mean(data(523:560,5:5)), length(data(523:560,5:5)));
                   complex(mean(data(561:637,5:5)), length(data(561:637,5:5)));
                   complex(mean(data(638:650,5:5)), length(data(638:650,5:5)));
                   complex(mean(data(651:726,5:5)), length(data(651:726,5:5)));
                   complex(mean(data(727:746,5:5)), length(data(727:746,5:5)));
                   complex(mean(data(747:845,5:5)), length(data(747:845,5:5)))];
               
       result6 = [complex(mean(data(1:46,6:6)), length(data(1:46,6:6)));
                   complex(mean(data(47:64,6:6)), length(data(47:64,6:6)));
                   complex(mean(data(65:129,6:6)), length(data(65:129,6:6)));
                   complex(mean(data(130:144,6:6)), length(data(130:144,6:6)));
                   complex(mean(data(145:258,6:6)), length(data(145:258,6:6)));
                   complex(mean(data(259:267,6:6)), length(data(259:267,6:6)));
                   complex(mean(data(268:324,6:6)), length(data(268:324,6:6)));
                   complex(mean(data(325:356,6:6)), length(data(325:356,6:6)));
                   complex(mean(data(357:442,6:6)), length(data(357:442,6:6)));
                   complex(mean(data(443:451,6:6)), length(data(443:451,6:6)));
                   complex(mean(data(452:530,6:6)), length(data(452:530,6:6)));
                   complex(mean(data(531:568,6:6)), length(data(531:568,6:6)));
                   complex(mean(data(569:636,6:6)), length(data(569:636,6:6)));
                   complex(mean(data(638:650,6:6)), length(data(638:650,6:6)));
                   complex(mean(data(651:709,6:6)), length(data(651:709,6:6)));
                   complex(mean(data(710:738,6:6)), length(data(710:738,6:6)));
                   complex(mean(data(739:845,6:6)), length(data(739:845,6:6)))];
               
        result7 = [complex(mean(data(1:46,7:7)), length(data(1:46,7:7)));
                   complex(mean(data(47:64,7:7)), length(data(47:64,7:7)));
                   complex(mean(data(65:142,7:7)), length(data(65:142,7:7)));
                   complex(mean(data(143:153,7:7)), length(data(143:153,7:7)));
                   complex(mean(data(154:258,7:7)), length(data(154:258,7:7)));
                   complex(mean(data(259:267,7:7)), length(data(259:267,7:7)));
                   complex(mean(data(268:324,7:7)), length(data(268:324,7:7)));
                   complex(mean(data(325:356,7:7)), length(data(325:356,7:7)));
                   complex(mean(data(357:442,7:7)), length(data(357:442,7:7)));
                   complex(mean(data(443:451,7:7)), length(data(443:451,7:7)));
                   complex(mean(data(452:534,7:7)), length(data(452:534,7:7)));
                   complex(mean(data(535:572,7:7)), length(data(535:572,7:7)));
                   complex(mean(data(573:637,7:7)), length(data(573:637,7:7)));
                   complex(mean(data(638:650,7:7)), length(data(638:650,7:7)));
                   complex(mean(data(651:726,7:7)), length(data(651:726,7:7)));
                   complex(mean(data(727:746,7:7)), length(data(727:746,7:7)));
                   complex(mean(data(747:845,7:7)), length(data(747:845,7:7)))];
               
               result = [result1 result2 result3 result4 result5 result6 result7];
         
     result11 = [complex(mean(dataDD(1:46,1:1)), length(dataDD(1:46,1:1)));
                   complex(mean(dataDD(47:64,1:1)), length(dataDD(47:64,1:1)));
                   complex(mean(dataDD(65:143,1:1)), length(dataDD(65:143,1:1)));
                   complex(mean(dataDD(144:159,1:1)), length(dataDD(144:159,1:1)));
                   complex(mean(dataDD(160:258,1:1)), length(dataDD(160:258,1:1)));
                   complex(mean(dataDD(259:267,1:1)), length(dataDD(259:267,1:1)));
                   complex(mean(dataDD(268:324,1:1)), length(dataDD(268:324,1:1)));
                   complex(mean(dataDD(325:356,1:1)), length(dataDD(325:356,1:1)));
                   complex(mean(dataDD(357:442,1:1)), length(dataDD(357:442,1:1)));
                   complex(mean(dataDD(443:451,1:1)), length(dataDD(443:451,1:1)));
                   complex(mean(dataDD(452:534,1:1)), length(dataDD(452:534,1:1)));
                   complex(mean(dataDD(535:572,1:1)), length(dataDD(535:572,1:1)));
                   complex(mean(dataDD(573:639,1:1)), length(dataDD(573:639,1:1)))];
               
     result22 = [complex(mean(dataDD(1:46,2:2)), length(dataDD(1:46,2:2)));
                   complex(mean(dataDD(47:64,2:2)), length(dataDD(47:64,2:2)));
                   complex(mean(dataDD(65:143,2:2)), length(dataDD(65:143,2:2)));
                   complex(mean(dataDD(144:159,2:2)), length(dataDD(144:159,2:2)));
                   complex(mean(dataDD(160:258,2:2)), length(dataDD(160:258,2:2)));
                   complex(mean(dataDD(259:267,2:2)), length(dataDD(259:267,2:2)));
                   complex(mean(dataDD(268:324,2:2)), length(dataDD(268:324,2:2)));
                   complex(mean(dataDD(325:356,2:2)), length(dataDD(325:356,2:2)));
                   complex(mean(dataDD(357:442,2:2)), length(dataDD(357:442,2:2)));
                   complex(mean(dataDD(443:451,2:2)), length(dataDD(443:451,2:2)));
                   complex(mean(dataDD(452:534,2:2)), length(dataDD(452:534,2:2)));
                   complex(mean(dataDD(535:572,2:2)), length(dataDD(535:572,2:2)));
                   complex(mean(dataDD(573:639,2:2)), length(dataDD(573:639,2:2)))];
               
     result33 = [complex(mean(dataDD(1:46,3:3)), length(dataDD(1:46,3:3)));
                   complex(mean(dataDD(47:64,3:3)), length(dataDD(47:64,3:3)));
                   complex(mean(dataDD(65:143,3:3)), length(dataDD(65:143,3:3)));
                   complex(mean(dataDD(144:159,3:3)), length(dataDD(144:159,3:3)));
                   complex(mean(dataDD(160:258,3:3)), length(dataDD(160:258,3:3)));
                   complex(mean(dataDD(259:267,3:3)), length(dataDD(259:267,3:3)));
                   complex(mean(dataDD(268:324,3:3)), length(dataDD(268:324,3:3)));
                   complex(mean(dataDD(325:356,3:3)), length(dataDD(325:356,3:3)));
                   complex(mean(dataDD(357:442,3:3)), length(dataDD(357:442,3:3)));
                   complex(mean(dataDD(443:451,3:3)), length(dataDD(443:451,3:3)));
                   complex(mean(dataDD(452:534,3:3)), length(dataDD(452:534,3:3)));
                   complex(mean(dataDD(535:572,3:3)), length(dataDD(535:572,3:3)));
                   complex(mean(dataDD(573:639,3:3)), length(dataDD(573:639,3:3)))];
               
     result44 = [complex(mean(dataDD(1:46,4:4)), length(dataDD(1:46,4:4)));
                   complex(mean(dataDD(47:64,4:4)), length(dataDD(47:64,4:4)));
                   complex(mean(dataDD(65:127,4:4)), length(dataDD(65:127,4:4)));
                   complex(mean(dataDD(128:142,4:4)), length(dataDD(128:142,4:4)));
                   complex(mean(dataDD(143:258,4:4)), length(dataDD(143:258,4:4)));
                   complex(mean(dataDD(259:267,4:4)), length(dataDD(259:267,4:4)));
                   complex(mean(dataDD(268:305,4:4)), length(dataDD(268:305,4:4)));
                   complex(mean(dataDD(306:339,4:4)), length(dataDD(306:339,4:4)));
                   complex(mean(dataDD(340:442,4:4)), length(dataDD(340:442,4:4)));
                   complex(mean(dataDD(443:451,4:4)), length(dataDD(443:451,4:4)));
                   complex(mean(dataDD(452:534,4:4)), length(dataDD(452:534,4:4)));
                   complex(mean(dataDD(535:572,4:4)), length(dataDD(535:572,4:4)));
                   complex(mean(dataDD(573:639,4:4)), length(dataDD(573:639,4:4)))];
               
     result55 = [complex(mean(dataDD(1:46,5:5)), length(dataDD(1:46,5:5)));
                   complex(mean(dataDD(47:64,5:5)), length(dataDD(47:64,5:5)));
                   complex(mean(dataDD(65:143,5:5)), length(dataDD(65:143,5:5)));
                   complex(mean(dataDD(144:159,5:5)), length(dataDD(144:159,5:5)));
                   complex(mean(dataDD(160:258,5:5)), length(dataDD(160:258,5:5)));
                   complex(mean(dataDD(259:267,5:5)), length(dataDD(259:267,5:5)));
                   complex(mean(dataDD(268:324,5:5)), length(dataDD(268:324,5:5)));
                   complex(mean(dataDD(325:356,5:5)), length(dataDD(325:356,5:5)));
                   complex(mean(dataDD(357:442,5:5)), length(dataDD(357:442,5:5)));
                   complex(mean(dataDD(443:451,5:5)), length(dataDD(443:451,5:5)));
                   complex(mean(dataDD(452:509,5:5)), length(dataDD(452:509,5:5)));
                   complex(mean(dataDD(510:547,5:5)), length(dataDD(510:547,5:5)));
                   complex(mean(dataDD(548:639,5:5)), length(dataDD(548:639,5:5)))];
               
     result66 = [complex(mean(dataDD(1:46,6:6)), length(dataDD(1:46,6:6)));
                   complex(mean(dataDD(47:64,6:6)), length(dataDD(47:64,6:6)));
                   complex(mean(dataDD(65:119,6:6)), length(dataDD(65:119,6:6)));
                   complex(mean(dataDD(120:134,6:6)), length(dataDD(120:134,6:6)));
                   complex(mean(dataDD(135:258,6:6)), length(dataDD(135:258,6:6)));
                   complex(mean(dataDD(259:267,6:6)), length(dataDD(259:267,6:6)));
                   complex(mean(dataDD(268:324,6:6)), length(dataDD(268:324,6:6)));
                   complex(mean(dataDD(325:356,6:6)), length(dataDD(325:356,6:6)));
                   complex(mean(dataDD(357:442,6:6)), length(dataDD(357:442,6:6)));
                   complex(mean(dataDD(443:451,6:6)), length(dataDD(443:451,6:6)));
                   complex(mean(dataDD(452:522,6:6)), length(dataDD(452:522,6:6)));
                   complex(mean(dataDD(523:560,6:6)), length(dataDD(523:560,6:6)));
                   complex(mean(dataDD(561:639,6:6)), length(dataDD(561:639,6:6)))];
               
      result77 = [complex(mean(dataDD(1:46,7:7)), length(dataDD(1:46,7:7)));
                   complex(mean(dataDD(47:64,7:7)), length(dataDD(47:64,7:7)));
                   complex(mean(dataDD(65:119,7:7)), length(dataDD(65:119,7:7)));
                   complex(mean(dataDD(120:134,7:7)), length(dataDD(120:134,7:7)));
                   complex(mean(dataDD(135:258,7:7)), length(dataDD(135:258,7:7)));
                   complex(mean(dataDD(259:267,7:7)), length(dataDD(259:267,7:7)));
                   complex(mean(dataDD(268:324,7:7)), length(dataDD(268:324,7:7)));
                   complex(mean(dataDD(325:356,7:7)), length(dataDD(325:356,7:7)));
                   complex(mean(dataDD(357:442,7:7)), length(dataDD(357:442,7:7)));
                   complex(mean(dataDD(443:451,7:7)), length(dataDD(443:451,7:7)));
                   complex(mean(dataDD(452:522,7:7)), length(dataDD(452:522,7:7)));
                   complex(mean(dataDD(523:560,7:7)), length(dataDD(523:560,7:7)));
                   complex(mean(dataDD(561:639,7:7)), length(dataDD(561:639,7:7)))];
               
           resultData = [result11 result22 result33 result44 result55 result66 result77];
           
    result111 = [complex(mean(dataDDD(1:46,1:1)), length(dataDDD(1:46,1:1)));
                   complex(mean(dataDDD(47:64,1:1)), length(dataDDD(47:64,1:1)));
                   complex(mean(dataDDD(65:143,1:1)), length(dataDDD(65:143,1:1)));
                   complex(mean(dataDDD(144:159,1:1)), length(dataDDD(144:159,1:1)));
                   complex(mean(dataDDD(160:258,1:1)), length(dataDDD(160:258,1:1)));
                   complex(mean(dataDDD(259:267,1:1)), length(dataDDD(259:267,1:1)));
                   complex(mean(dataDDD(268:324,1:1)), length(dataDDD(268:324,1:1)));
                   complex(mean(dataDDD(325:356,1:1)), length(dataDDD(325:356,1:1)));
                   complex(mean(dataDDD(357:445,1:1)), length(dataDDD(357:445,1:1)))];
               
     
    result222 = [complex(mean(dataDDD(1:46,2:2)), length(dataDDD(1:46,2:2)));
                   complex(mean(dataDDD(47:64,2:2)), length(dataDDD(47:64,2:2)));
                   complex(mean(dataDDD(65:143,2:2)), length(dataDDD(65:143,2:2)));
                   complex(mean(dataDDD(144:159,2:2)), length(dataDDD(144:159,2:2)));
                   complex(mean(dataDDD(160:258,2:2)), length(dataDDD(160:258,2:2)));
                   complex(mean(dataDDD(259:267,2:2)), length(dataDDD(259:267,2:2)));
                   complex(mean(dataDDD(268:324,2:2)), length(dataDDD(268:324,2:2)));
                   complex(mean(dataDDD(325:356,2:2)), length(dataDDD(325:356,2:2)));
                   complex(mean(dataDDD(357:445,2:2)), length(dataDDD(357:445,2:2)))];
               
    result333 = [complex(mean(dataDDD(1:46,3:3)), length(dataDDD(1:46,3:3)));
                   complex(mean(dataDDD(47:64,3:3)), length(dataDDD(47:64,3:3)));
                   complex(mean(dataDDD(65:143,3:3)), length(dataDDD(65:143,3:3)));
                   complex(mean(dataDDD(144:159,3:3)), length(dataDDD(144:159,3:3)));
                   complex(mean(dataDDD(160:258,3:3)), length(dataDDD(160:258,3:3)));
                   complex(mean(dataDDD(259:267,3:3)), length(dataDDD(259:267,3:3)));
                   complex(mean(dataDDD(268:324,3:3)), length(dataDDD(268:324,3:3)));
                   complex(mean(dataDDD(325:356,3:3)), length(dataDDD(325:356,3:3)));
                   complex(mean(dataDDD(357:445,3:3)), length(dataDDD(357:445,3:3)))];
               
     result444 = [complex(mean(dataDDD(1:46,4:4)), length(dataDDD(1:46,4:4)));
                   complex(mean(dataDDD(47:64,4:4)), length(dataDDD(47:64,4:4)));
                   complex(mean(dataDDD(65:132,4:4)), length(dataDDD(65:132,4:4)));
                   complex(mean(dataDDD(133:147,4:4)), length(dataDDD(133:147,4:4)));
                   complex(mean(dataDDD(148:258,4:4)), length(dataDDD(148:258,4:4)));
                   complex(mean(dataDDD(259:267,4:4)), length(dataDDD(259:267,4:4)));
                   complex(mean(dataDDD(268:324,4:4)), length(dataDDD(268:324,4:4)));
                   complex(mean(dataDDD(325:356,4:4)), length(dataDDD(325:356,4:4)));
                   complex(mean(dataDDD(357:445,4:4)), length(dataDDD(357:445,4:4)))];
               
    result555 = [complex(mean(dataDDD(1:46,5:5)), length(dataDDD(1:46,5:5)));
                   complex(mean(dataDDD(47:64,5:5)), length(dataDDD(47:64,5:5)));
                   complex(mean(dataDDD(65:143,5:5)), length(dataDDD(65:143,5:5)));
                   complex(mean(dataDDD(144:159,5:5)), length(dataDDD(144:159,5:5)));
                   complex(mean(dataDDD(160:258,5:5)), length(dataDDD(160:258,5:5)));
                   complex(mean(dataDDD(259:267,5:5)), length(dataDDD(259:267,5:5)));
                   complex(mean(dataDDD(268:309,5:5)), length(dataDDD(268:309,5:5)));
                   complex(mean(dataDDD(310:344,5:5)), length(dataDDD(310:344,5:5)));
                   complex(mean(dataDDD(345:445,5:5)), length(dataDDD(345:445,5:5)))];
               
   result667 = [complex(mean(dataDDD(1:46,6:6)), length(dataDDD(1:46,6:6)));
                   complex(mean(dataDDD(47:64,6:6)), length(dataDDD(47:64,6:6)));
                   complex(mean(dataDDD(65:128,6:6)), length(dataDDD(65:128,6:6)));
                   complex(mean(dataDDD(129:143,6:6)), length(dataDDD(129:143,6:6)));
                   complex(mean(dataDDD(144:258,6:6)), length(dataDDD(144:258,6:6)));
                   complex(mean(dataDDD(259:267,6:6)), length(dataDDD(259:267,6:6)));
                   complex(mean(dataDDD(268:311,6:6)), length(dataDDD(268:311,6:6)));
                   complex(mean(dataDDD(312:345,6:6)), length(dataDDD(312:345,6:6)));
                   complex(mean(dataDDD(346:445,6:6)), length(dataDDD(346:445,6:6)))];
               
    result777 = [complex(mean(dataDDD(1:46,7:7)), length(dataDDD(1:46,7:7)));
                   complex(mean(dataDDD(47:64,7:7)), length(dataDDD(47:64,7:7)));
                   complex(mean(dataDDD(65:143,7:7)), length(dataDDD(65:143,7:7)));
                   complex(mean(dataDDD(144:159,7:7)), length(dataDDD(144:159,7:7)));
                   complex(mean(dataDDD(160:258,7:7)), length(dataDDD(160:258,7:7)));
                   complex(mean(dataDDD(259:267,7:7)), length(dataDDD(259:267,7:7)));
                   complex(mean(dataDDD(268:324,7:7)), length(dataDDD(268:324,7:7)));
                   complex(mean(dataDDD(325:356,7:7)), length(dataDDD(325:356,7:7)));
                   complex(mean(dataDDD(357:445,7:7)), length(dataDDD(357:445,7:7)));];
     
            resultData2 = [result111 result222 result333 result444 result555 result667 result777];
            
    resultD41 = [complex(mean(dataD4(1:46,1:1)), length(dataD4(1:46,1:1)));
                   complex(mean(dataD4(47:79,1:1)), length(dataD4(47:79,1:1)));
                   complex(mean(dataD4(80:144,1:1)), length(dataD4(80:144,1:1)));
                   complex(mean(dataD4(145:163,1:1)), length(dataD4(145:163,1:1)));
                   complex(mean(dataD4(164:258,1:1)), length(dataD4(164:258,1:1)))];
               
    resultD42 = [complex(mean(dataD4(1:46,2:2)), length(dataD4(1:46,2:2)));
                   complex(mean(dataD4(47:79,2:2)), length(dataD4(47:79,2:2)));
                   complex(mean(dataD4(80:144,2:2)), length(dataD4(80:144,2:2)));
                   complex(mean(dataD4(145:163,2:2)), length(dataD4(145:163,2:2)));
                   complex(mean(dataD4(164:258,2:2)), length(dataD4(164:258,2:2)))];
               
    resultD43 = [complex(mean(dataD4(1:46,3:3)), length(dataD4(1:46,3:3)));
                   complex(mean(dataD4(47:79,3:3)), length(dataD4(47:79,3:3)));
                   complex(mean(dataD4(80:144,3:3)), length(dataD4(80:144,3:3)));
                   complex(mean(dataD4(145:163,3:3)), length(dataD4(145:163,3:3)));
                   complex(mean(dataD4(164:258,3:3)), length(dataD4(164:258,3:3)))];
               
     resultD44 = [complex(mean(dataD4(1:46,4:4)), length(dataD4(1:46,4:4)));
                   complex(mean(dataD4(47:79,4:4)), length(dataD4(47:79,4:4)));
                   complex(mean(dataD4(80:129,4:4)), length(dataD4(80:129,4:4)));
                   complex(mean(dataD4(130:144,4:4)), length(dataD4(130:144,4:4)));
                   complex(mean(dataD4(145:258,4:4)), length(dataD4(145:258,4:4)))];
               
     resultD45 = [complex(mean(dataD4(1:46,5:5)), length(dataD4(1:46,5:5)));
                   complex(mean(dataD4(47:79,5:5)), length(dataD4(47:79,5:5)));
                   complex(mean(dataD4(80:144,5:5)), length(dataD4(80:144,5:5)));
                   complex(mean(dataD4(145:163,5:5)), length(dataD4(145:163,5:5)));
                   complex(mean(dataD4(164:258,5:5)), length(dataD4(164:258,5:5)))];

             resultDataD4 = [resultD41 resultD42 resultD43 resultD44 resultD45];  
end