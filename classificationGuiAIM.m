function varargout = classificationGuiAIM(varargin)
% CLASSIFICATIONGUIAIM MATLAB code for classificationGuiAIM.fig
%      CLASSIFICATIONGUIAIM, by itself, creates a new CLASSIFICATIONGUIAIM or raises the existing
%      singleton*.
%
%      H = CLASSIFICATIONGUIAIM returns the handle to a new CLASSIFICATIONGUIAIM or the handle to
%      the existing singleton*.
%
%      CLASSIFICATIONGUIAIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLASSIFICATIONGUIAIM.M with the given input arguments.
%
%      CLASSIFICATIONGUIAIM('Property','Value',...) creates a new CLASSIFICATIONGUIAIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before classificationGuiAIM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to classificationGuiAIM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help classificationGuiAIM

% Last Modified by GUIDE v2.5 09-Oct-2019 15:51:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @classificationGuiAIM_OpeningFcn, ...
                   'gui_OutputFcn',  @classificationGuiAIM_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before classificationGuiAIM is made visible.
function classificationGuiAIM_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to classificationGuiAIM (see VARARGIN)

% Choose default command line output for classificationGuiAIM
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes classificationGuiAIM wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = classificationGuiAIM_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in aim.
function aim_Callback(hObject, eventdata, handles)
% hObject    handle to aim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
modo = get(handles.aim,'Tag')
switch modo
    case 'aim'
        close classificationGui;
        run ('classificationGuiAIM');
end

% --- Executes on button press in wim.
function wim_Callback(hObject, eventdata, handles)
% hObject    handle to wim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

modo = get(handles.wim, 'Tag')
switch modo
    case 'wim'
        close classificationGuiAIM;
        run ('classificationGui');
end


% --- Executes on button press in file.
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guidata(gcbf,handles);

[file path] = uigetfile({'*.txt'}, 'File Selector');

ll = load(fullfile(path,file));

 axes(handles.chartAIM1);
 plot(ll);
 xlabel('Time (s)');
 ylabel('\theta_r (º)');
    

 dataSensorgram = (ll(:,1:1));
 handles.data=dataSensorgram;
 guidata(handles.calc, handles);


% --- Executes during object creation, after setting all properties.
function chartAIM1_CreateFcn(hObject, eventdata, handles)
xlabel('Time (s)');
ylabel('\theta_r (º)');


% --- Executes on button press in gaussian.
function gaussian_Callback(hObject, eventdata, handles)

% --- Executes on button press in axisY.
function axisY_Callback(hObject, eventdata, handles)

% --- Executes on button press in temporalC.
function temporalC_Callback(hObject, eventdata, handles)

function valueCalc_Callback(hObject, eventdata, handles)
% Hints: get(hObject,'String') returns contents of valueCalc as text
%        str2double(get(hObject,'String')) returns contents of valueCalc as a double


% --- Executes during object creation, after setting all properties.
function valueCalc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to valueCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in calc.
function calc_Callback(hObject, eventdata, handles)
% hObject    handle to calc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
valueCalc = str2num(get(findobj(gcf, 'Tag', 'valueCalc'), 'String'));
guidata(gcbf,handles);

valueGaussian = get(handles.gaussian, 'Value');
valueTemporalC = get(handles.temporalC, 'Value');
valueAxisY = get(handles.axisY, 'Value');
data_ll = handles.data;


if (valueGaussian == 1 && valueTemporalC == 0 && valueAxisY == 0)
%% Calculo para fazer com ruido gaussiano
    
elseif (valueGaussian == 0 && valueTemporalC == 1 && valueAxisY == 0)
%% Calculo para fazer com o aumento temporal
    
elseif (valueGaussian == 0 && valueTemporalC == 0 && valueAxisY == 1)
%% Calculo para fazer com o aumento do eixo Y
 result = get_axisY(data_ll, valueCalc);
    
    %Codigo da Regressão Linear
    
    [IC_A, IC_B, coef_A] = get_resultRegress(5, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
        
        %Grafico no Modo WIM das regiões dos regimes permanentes
        axes(handles.chartAIM2);
        plot(result);
        hold on;
        plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\theta_r (º)');
                
        %Grafico no Modo WIM das regiões de transição
        axes(handles.chartAIM3);
        plot(result);
        hold on;
        plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
        xlabel('Tempo (s)');
        ylabel('\theta_r (º)');

        figure(1)
        plot(IC_A+result(1), 'b');
        hold on;
        plot(IC_B+result(1), 'r');
        hold on;
        plot(coef_A+result(1), 'k');
    
elseif (valueGaussian == 0 && valueTemporalC == 0 && valueAxisY == 0)
%% Calculo para fazer sem nenhuma modificação
    result = data_ll;
    
    [IC_A, IC_B, coef_A] = get_resultRegress(10, result);
    
    idx = IC_A <= 0 & IC_B >=0;
    input_date_regress = result.*idx;
    
    idx2 = (IC_A <= 0 & IC_B <= 0) | (IC_A >= 0 & IC_B >= 0);
    input_date_regress2 = result.*idx2;
    
    axes(handles.chartAIM2);
    plot(result);
    hold on;
    plot(find(idx==1), input_date_regress(find(idx==1)),'ro');
    xlabel('Tempo (s)');
    ylabel('\theta_r (º)');
    
    %Grafico no Modo WIM das regiões de transição
    axes(handles.chartAIM3);
    plot(result);
    hold on;
    plot(find(idx2==1), input_date_regress2(find(idx2==1)),'ro');
    xlabel('Tempo (s)');
    ylabel('\theta_r (º)');
    
    figure(1)
    plot(IC_A+result(1), 'b');
    hold on;
    plot(IC_B+result(1), 'r');
    hold on;
    plot(coef_A+result(1), 'k');

end


% --- Executes during object creation, after setting all properties.
function chartAIM2_CreateFcn(hObject, eventdata, handles)
xlabel('Time (s)');
ylabel('\theta_r (º)');


% --- Executes during object creation, after setting all properties.
function chartAIM3_CreateFcn(hObject, eventdata, handles)
xlabel('Time (s)');
ylabel('\theta_r (º)');



function resultString_Callback(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resultString as text
%        str2double(get(hObject,'String')) returns contents of resultString as a double


% --- Executes during object creation, after setting all properties.
function resultString_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resultString (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function chartAIM4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chartAIM4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate chartAIM4
