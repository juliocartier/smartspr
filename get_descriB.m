function [result, resultData2, resultData3, resultDataB4] = get_descriB(data, data2, dataBBB, dataB4)

         result1 = [complex(mean(data(1:46,1:1)), length(data(1:46,1:1)));
                   complex(mean(data(47:58,1:1)), length(data(47:58,1:1)));
                   complex(mean(data(59:133,1:1)), length(data(59:133,1:1)));
                   complex(mean(data(134:162,1:1)), length(data(134:162,1:1)));
                   complex(mean(data(163:252,1:1)), length(data(163:252,1:1)));
                   complex(mean(data(253:262,1:1)), length(data(253:262,1:1)));
                   complex(mean(data(263:322,1:1)), length(data(263:322,1:1)));
                   complex(mean(data(323:359,1:1)), length(data(323:359,1:1)));
                   complex(mean(data(360:446,1:1)), length(data(360:446,1:1)));
                   complex(mean(data(447:467,1:1)), length(data(447:467,1:1)));
                   complex(mean(data(468:546,1:1)), length(data(468:546,1:1)));
                   complex(mean(data(547:564,1:1)), length(data(547:564,1:1)));
                   complex(mean(data(565:661,1:1)), length(data(565:661,1:1)));
                   complex(mean(data(662:671,1:1)), length(data(662:671,1:1)));
                   complex(mean(data(672:727,1:1)), length(data(672:727,1:1)));
                   complex(mean(data(728:760,1:1)), length(data(728:760,1:1)));
                   complex(mean(data(761:845,1:1)), length(data(761:845,1:1)))];
               
        result2 = [complex(mean(data(1:46,2:2)), length(data(1:46,2:2)));
                   complex(mean(data(47:58,2:2)), length(data(47:58,2:2)));
                   complex(mean(data(59:133,2:2)), length(data(59:133,2:2)));
                   complex(mean(data(134:162,2:2)), length(data(134:162,2:2)));
                   complex(mean(data(163:252,2:2)), length(data(163:252,2:2)));
                   complex(mean(data(253:262,2:2)), length(data(253:262,2:2)));
                   complex(mean(data(263:343,2:2)), length(data(263:343,2:2)));
                   complex(mean(data(344:381,2:2)), length(data(344:381,2:2)));
                   complex(mean(data(382:446,2:2)), length(data(382:446,2:2)));
                   complex(mean(data(447:467,2:2)), length(data(447:467,2:2)));
                   complex(mean(data(468:546,2:2)), length(data(468:546,2:2)));
                   complex(mean(data(547:564,2:2)), length(data(547:564,2:2)));
                   complex(mean(data(565:661,2:2)), length(data(565:661,2:2)));
                   complex(mean(data(662:671,2:2)), length(data(662:671,2:2)));
                   complex(mean(data(672:727,2:2)), length(data(672:727,2:2)));
                   complex(mean(data(728:760,2:2)), length(data(728:760,2:2)));
                   complex(mean(data(761:845,2:2)), length(data(761:845,2:2)))];
               
        result3 = [complex(mean(data(1:46,3:3)), length(data(1:46,3:3)));
                   complex(mean(data(47:58,3:3)), length(data(47:58,3:3)));
                   complex(mean(data(59:133,3:3)), length(data(59:133,3:3)));
                   complex(mean(data(134:162,3:3)), length(data(134:162,3:3)));
                   complex(mean(data(163:252,3:3)), length(data(163:252,3:3)));
                   complex(mean(data(253:262,3:3)), length(data(253:262,3:3)));
                   complex(mean(data(263:343,3:3)), length(data(263:343,3:3)));
                   complex(mean(data(344:381,3:3)), length(data(344:381,3:3)));
                   complex(mean(data(382:446,3:3)), length(data(382:446,3:3)));
                   complex(mean(data(447:467,3:3)), length(data(447:467,3:3)));
                   complex(mean(data(468:546,3:3)), length(data(468:546,3:3)));
                   complex(mean(data(547:564,3:3)), length(data(547:564,3:3)));
                   complex(mean(data(565:661,3:3)), length(data(565:661,3:3)));
                   complex(mean(data(662:671,3:3)), length(data(662:671,3:3)));
                   complex(mean(data(672:727,3:3)), length(data(672:727,3:3)));
                   complex(mean(data(728:760,3:3)), length(data(728:760,3:3)));
                   complex(mean(data(761:845,3:3)), length(data(761:845,3:3)))];
               
        result4 = [complex(mean(data(1:46,4:4)), length(data(1:46,4:4)));
                   complex(mean(data(47:58,4:4)), length(data(47:58,4:4)));
                   complex(mean(data(59:114,4:4)), length(data(59:114,4:4)));
                   complex(mean(data(115:142,4:4)), length(data(115:142,4:4)));
                   complex(mean(data(143:252,4:4)), length(data(143:252,4:4)));
                   complex(mean(data(253:262,4:4)), length(data(253:262,4:4)));
                   complex(mean(data(263:343,4:4)), length(data(263:343,4:4)));
                   complex(mean(data(344:381,4:4)), length(data(344:381,4:4)));
                   complex(mean(data(382:446,4:4)), length(data(382:446,4:4)));
                   complex(mean(data(447:467,4:4)), length(data(447:467,4:4)));
                   complex(mean(data(468:546,4:4)), length(data(468:546,4:4)));
                   complex(mean(data(547:564,4:4)), length(data(547:564,4:4)));
                   complex(mean(data(565:661,4:4)), length(data(565:661,4:4)));
                   complex(mean(data(662:671,4:4)), length(data(662:671,4:4)));
                   complex(mean(data(672:713,4:4)), length(data(672:713,4:4)));
                   complex(mean(data(714:746,4:4)), length(data(714:746,4:4)));
                   complex(mean(data(747:845,4:4)), length(data(747:845,4:4)))];
           
         result5 = [complex(mean(data(1:46,5:5)), length(data(1:46,5:5)));
                   complex(mean(data(47:58,5:5)), length(data(47:58,5:5)));
                   complex(mean(data(59:133,5:5)), length(data(59:133,5:5)));
                   complex(mean(data(134:162,5:5)), length(data(134:162,5:5)));
                   complex(mean(data(163:252,5:5)), length(data(163:252,5:5)));
                   complex(mean(data(253:262,5:5)), length(data(253:262,5:5)));
                   complex(mean(data(263:343,5:5)), length(data(263:343,5:5)));
                   complex(mean(data(344:381,5:5)), length(data(344:381,5:5)));
                   complex(mean(data(382:446,5:5)), length(data(382:446,5:5)));
                   complex(mean(data(447:467,5:5)), length(data(447:467,5:5)));
                   complex(mean(data(468:526,5:5)), length(data(468:526,5:5)));
                   complex(mean(data(527:543,5:5)), length(data(527:543,5:5)));
                   complex(mean(data(544:661,5:5)), length(data(544:661,5:5)));
                   complex(mean(data(662:671,5:5)), length(data(662:671,5:5)));
                   complex(mean(data(672:727,5:5)), length(data(672:727,5:5)));
                   complex(mean(data(728:760,5:5)), length(data(728:760,5:5)));
                   complex(mean(data(761:845,5:5)), length(data(761:845,5:5)))];
        
        result6 = [complex(mean(data(1:46,6:6)), length(data(1:46,6:6)));
                   complex(mean(data(47:58,6:6)), length(data(47:58,6:6)));
                   complex(mean(data(59:117,6:6)), length(data(59:117,6:6)));
                   complex(mean(data(118:146,6:6)), length(data(118:146,6:6)));
                   complex(mean(data(147:251,6:6)), length(data(147:251,6:6)));
                   complex(mean(data(252:262,6:6)), length(data(252:262,6:6)));
                   complex(mean(data(263:343,6:6)), length(data(263:343,6:6)));
                   complex(mean(data(344:381,6:6)), length(data(344:381,6:6)));
                   complex(mean(data(382:446,6:6)), length(data(382:446,6:6)));
                   complex(mean(data(447:467,6:6)), length(data(447:467,6:6)));
                   complex(mean(data(468:532,6:6)), length(data(468:532,6:6)));
                   complex(mean(data(533:549,6:6)), length(data(533:549,6:6)));
                   complex(mean(data(550:661,6:6)), length(data(550:661,6:6)));
                   complex(mean(data(662:671,6:6)), length(data(662:671,6:6)));
                   complex(mean(data(672:704,6:6)), length(data(672:704,6:6)));
                   complex(mean(data(705:738,6:6)), length(data(705:738,6:6)));
                   complex(mean(data(739:845,6:6)), length(data(739:845,6:6)))];
       
        result7 = [complex(mean(data(1:46,7:7)), length(data(1:46,7:7)));
                   complex(mean(data(47:58,7:7)), length(data(47:58,7:7)));
                   complex(mean(data(59:133,7:7)), length(data(59:133,7:7)));
                   complex(mean(data(134:162,7:7)), length(data(134:162,7:7)));
                   complex(mean(data(163:252,7:7)), length(data(163:252,7:7)));
                   complex(mean(data(253:262,7:7)), length(data(253:262,7:7)));
                   complex(mean(data(263:343,7:7)), length(data(263:343,7:7)));
                   complex(mean(data(344:381,7:7)), length(data(344:381,7:7)));
                   complex(mean(data(382:446,7:7)), length(data(382:446,7:7)));
                   complex(mean(data(447:467,7:7)), length(data(447:467,7:7)));
                   complex(mean(data(468:526,7:7)), length(data(468:526,7:7)));
                   complex(mean(data(527:543,7:7)), length(data(527:543,7:7)));
                   complex(mean(data(544:661,7:7)), length(data(544:661,7:7)));
                   complex(mean(data(662:671,7:7)), length(data(662:671,7:7)));
                   complex(mean(data(672:727,7:7)), length(data(672:727,7:7)));
                   complex(mean(data(728:760,7:7)), length(data(728:760,7:7)));
                   complex(mean(data(761:845,7:7)), length(data(761:845,7:7 )))];
                
               result = [result1 result2 result3 result4 result5 result6 result7];
                
      result11 = [complex(mean(data2(1:46,1:1)), length(data2(1:46,1:1)));
                   complex(mean(data2(47:58,1:1)), length(data2(47:58,1:1)));
                   complex(mean(data2(59:133,1:1)), length(data2(59:133,1:1)));
                   complex(mean(data2(134:162,1:1)), length(data2(134:162,1:1)));
                   complex(mean(data2(163:252,1:1)), length(data2(163:252,1:1)));
                   complex(mean(data2(253:262,1:1)), length(data2(253:262,1:1)));
                   complex(mean(data2(263:343,1:1)), length(data2(263:343,1:1)));
                   complex(mean(data2(344:381,1:1)), length(data2(344:381,1:1)));
                   complex(mean(data2(382:446,1:1)), length(data2(382:446,1:1)));
                   complex(mean(data2(447:467,1:1)), length(data2(447:467,1:1)));
                   complex(mean(data2(468:546,1:1)), length(data2(468:546,1:1)));
                   complex(mean(data2(547:564,1:1)), length(data2(547:564,1:1)));
                   complex(mean(data2(565:661,1:1)), length(data2(565:661,1:1)))];
               
       result22 = [complex(mean(data2(1:46,2:2)), length(data2(1:46,2:2)));
                   complex(mean(data2(47:58,2:2)), length(data2(47:58,2:2)));
                   complex(mean(data2(59:133,2:2)), length(data2(59:133,2:2)));
                   complex(mean(data2(134:162,2:2)), length(data2(134:162,2:2)));
                   complex(mean(data2(163:252,2:2)), length(data2(163:252,2:2)));
                   complex(mean(data2(253:262,2:2)), length(data2(253:262,2:2)));
                   complex(mean(data2(263:343,2:2)), length(data2(263:343,2:2)));
                   complex(mean(data2(344:381,2:2)), length(data2(344:381,2:2)));
                   complex(mean(data2(382:446,2:2)), length(data2(382:446,2:2)));
                   complex(mean(data2(447:467,2:2)), length(data2(447:467,2:2)));
                   complex(mean(data2(468:546,2:2)), length(data2(468:546,2:2)));
                   complex(mean(data2(547:564,2:2)), length(data2(547:564,2:2)));
                   complex(mean(data2(565:661,2:2)), length(data2(565:661,2:2)))];
               
       result33 = [complex(mean(data2(1:46,3:3)), length(data2(1:46,3:3)));
                   complex(mean(data2(47:58,3:3)), length(data2(47:58,3:3)));
                   complex(mean(data2(59:133,3:3)), length(data2(59:133,3:3)));
                   complex(mean(data2(134:162,3:3)), length(data2(134:162,3:3)));
                   complex(mean(data2(163:252,3:3)), length(data2(163:252,3:3)));
                   complex(mean(data2(253:262,3:3)), length(data2(253:262,3:3)));
                   complex(mean(data2(263:343,3:3)), length(data2(263:343,3:3)));
                   complex(mean(data2(344:381,3:3)), length(data2(344:381,3:3)));
                   complex(mean(data2(382:446,3:3)), length(data2(382:446,3:3)));
                   complex(mean(data2(447:467,3:3)), length(data2(447:467,3:3)));
                   complex(mean(data2(468:546,3:3)), length(data2(468:546,3:3)));
                   complex(mean(data2(547:564,3:3)), length(data2(547:564,3:3)));
                   complex(mean(data2(565:661,3:3)), length(data2(565:661,3:3)))];
               
       result44 = [complex(mean(data2(1:46,4:4)), length(data2(1:46,4:4)));
                   complex(mean(data2(47:58,4:4)), length(data2(47:58,4:4)));
                   complex(mean(data2(59:126,4:4)), length(data2(59:126,4:4)));
                   complex(mean(data2(127:155,4:4)), length(data2(127:155,4:4)));
                   complex(mean(data2(156:251,4:4)), length(data2(156:251,4:4)));
                   complex(mean(data2(253:262,4:4)), length(data2(253:262,4:4)));
                   complex(mean(data2(263:343,4:4)), length(data2(263:343,4:4)));
                   complex(mean(data2(344:381,4:4)), length(data2(344:381,4:4)));
                   complex(mean(data2(382:446,4:4)), length(data2(382:446,4:4)));
                   complex(mean(data2(447:467,4:4)), length(data2(447:467,4:4)));
                   complex(mean(data2(468:532,4:4)), length(data2(468:532,4:4)));
                   complex(mean(data2(533:549,4:4)), length(data2(533:549,4:4)));
                   complex(mean(data2(550:661,4:4)), length(data2(550:661,4:4)))];
        
       result55 = [complex(mean(data2(1:46,5:5)), length(data2(1:46,5:5)));
                   complex(mean(data2(47:58,5:5)), length(data2(47:58,5:5)));
                   complex(mean(data2(59:115,5:5)), length(data2(59:115,5:5)));
                   complex(mean(data2(116:144,5:5)), length(data2(116:144,5:5)));
                   complex(mean(data2(145:252,5:5)), length(data2(145:252,5:5)));
                   complex(mean(data2(253:262,5:5)), length(data2(253:262,5:5)));
                   complex(mean(data2(263:343,5:5)), length(data2(263:343,5:5)));
                   complex(mean(data2(344:381,5:5)), length(data2(344:381,5:5)));
                   complex(mean(data2(382:446,5:5)), length(data2(382:446,5:5)));
                   complex(mean(data2(447:467,5:5)), length(data2(447:467,5:5)));
                   complex(mean(data2(468:546,5:5)), length(data2(468:546,5:5)));
                   complex(mean(data2(547:564,5:5)), length(data2(547:564,5:5)));
                   complex(mean(data2(565:661,5:5)), length(data2(565:661,5:5)))];
        
       result66 = [complex(mean(data2(1:46,6:6)), length(data2(1:46,6:6)));
                   complex(mean(data2(47:58,6:6)), length(data2(47:58,6:6)));
                   complex(mean(data2(59:133,6:6)), length(data2(59:133,6:6)));
                   complex(mean(data2(134:162,6:6)), length(data2(134:162,6:6)));
                   complex(mean(data2(163:252,6:6)), length(data2(163:252,6:6)));
                   complex(mean(data2(253:262,6:6)), length(data2(253:262,6:6)));
                   complex(mean(data2(263:329,6:6)), length(data2(263:329,6:6)));
                   complex(mean(data2(330:365,6:6)), length(data2(330:365,6:6)));
                   complex(mean(data2(366:445,6:6)), length(data2(366:445,6:6)));
                   complex(mean(data2(446:467,6:6)), length(data2(446:467,6:6)));
                   complex(mean(data2(468:526,6:6)), length(data2(468:526,6:6)));
                   complex(mean(data2(527:544,6:6)), length(data2(527:544,6:6)));
                   complex(mean(data2(545:661,6:6)), length(data2(545:661,6:6)))];

      result77 = [complex(mean(data2(1:46,7:7)), length(data2(1:46,7:7)));
                   complex(mean(data2(47:58,7:7)), length(data2(47:58,7:7)));
                   complex(mean(data2(59:126,7:7)), length(data2(59:126,7:7)));
                   complex(mean(data2(127:155,7:7)), length(data2(127:155,7:7)));
                   complex(mean(data2(156:251,7:7)), length(data2(156:251,7:7)));
                   complex(mean(data2(253:262,7:7)), length(data2(253:262,7:7)));
                   complex(mean(data2(263:343,7:7)), length(data2(263:343,7:7)));
                   complex(mean(data2(344:381,7:7)), length(data2(344:381,7:7)));
                   complex(mean(data2(382:446,7:7)), length(data2(382:446,7:7)));
                   complex(mean(data2(447:467,7:7)), length(data2(447:467,7:7)));
                   complex(mean(data2(468:532,7:7)), length(data2(468:532,7:7)));
                   complex(mean(data2(533:549,7:7)), length(data2(533:549,7:7)));
                   complex(mean(data2(550:661,7:7)), length(data2(550:661,7:7)))];
               
              
       resultData2 = [result11 result22 result33 result44 result55 result66 result77];
       
 result111 = [complex(mean(dataBBB(1:46,1:1)), length(dataBBB(1:46,1:1)));
                   complex(mean(dataBBB(47:58,1:1)), length(dataBBB(47:58,1:1)));
                   complex(mean(dataBBB(59:133,1:1)), length(dataBBB(59:133,1:1)));
                   complex(mean(dataBBB(134:162,1:1)), length(dataBBB(134:162,1:1)));
                   complex(mean(dataBBB(163:252,1:1)), length(dataBBB(163:252,1:1)));
                   complex(mean(dataBBB(253:262,1:1)), length(dataBBB(253:262,1:1)));
                   complex(mean(dataBBB(263:343,1:1)), length(dataBBB(263:343,1:1)));
                   complex(mean(dataBBB(344:381,1:1)), length(dataBBB(344:381,1:1)));
                   complex(mean(dataBBB(382:449,1:1)), length(dataBBB(382:449,1:1)));];
  
 result222 = [complex(mean(dataBBB(1:46,2:2)), length(dataBBB(1:46,2:2)));
                   complex(mean(dataBBB(47:58,2:2)), length(dataBBB(47:58,2:2)));
                   complex(mean(dataBBB(59:133,2:2)), length(dataBBB(59:133,2:2)));
                   complex(mean(dataBBB(134:162,2:2)), length(dataBBB(134:162,2:2)));
                   complex(mean(dataBBB(163:252,2:2)), length(dataBBB(163:252,2:2)));
                   complex(mean(dataBBB(253:262,2:2)), length(dataBBB(253:262,2:2)));
                   complex(mean(dataBBB(263:343,2:2)), length(dataBBB(263:343,2:2)));
                   complex(mean(dataBBB(344:381,2:2)), length(dataBBB(344:381,2:2)));
                   complex(mean(dataBBB(382:449,2:2)), length(dataBBB(382:449,2:2)));];
    
 result333 = [complex(mean(dataBBB(1:46,3:3)), length(dataBBB(1:46,3:3)));
                   complex(mean(dataBBB(47:58,3:3)), length(dataBBB(47:58,3:3)));
                   complex(mean(dataBBB(59:133,3:3)), length(dataBBB(59:133,3:3)));
                   complex(mean(dataBBB(134:162,3:3)), length(dataBBB(134:162,3:3)));
                   complex(mean(dataBBB(163:252,3:3)), length(dataBBB(163:252,3:3)));
                   complex(mean(dataBBB(253:262,3:3)), length(dataBBB(253:262,3:3)));
                   complex(mean(dataBBB(263:343,3:3)), length(dataBBB(263:343,3:3)));
                   complex(mean(dataBBB(344:381,3:3)), length(dataBBB(344:381,3:3)));
                   complex(mean(dataBBB(382:449,3:3)), length(dataBBB(382:449,3:3)));];
               
result444 = [complex(mean(dataBBB(1:46,4:4)), length(dataBBB(1:46,4:4)));
                   complex(mean(dataBBB(47:58,4:4)), length(dataBBB(47:58,4:4)));
                   complex(mean(dataBBB(59:133,4:4)), length(dataBBB(59:133,4:4)));
                   complex(mean(dataBBB(134:162,4:4)), length(dataBBB(134:162,4:4)));
                   complex(mean(dataBBB(163:252,4:4)), length(dataBBB(163:252,4:4)));
                   complex(mean(dataBBB(253:262,4:4)), length(dataBBB(253:262,4:4)));
                   complex(mean(dataBBB(263:318,4:4)), length(dataBBB(263:318,4:4)));
                   complex(mean(dataBBB(319:356,4:4)), length(dataBBB(319:356,4:4)));
                   complex(mean(dataBBB(357:449,4:4)), length(dataBBB(357:449,4:4)))];
               
 result555 = [complex(mean(dataBBB(1:46,5:5)), length(dataBBB(1:46,5:5)));
                   complex(mean(dataBBB(47:58,5:5)), length(dataBBB(47:58,5:5)));
                   complex(mean(dataBBB(59:122,5:5)), length(dataBBB(59:122,5:5)));
                   complex(mean(dataBBB(123:151,5:5)), length(dataBBB(123:151,5:5)));
                   complex(mean(dataBBB(152:252,5:5)), length(dataBBB(152:252,5:5)));
                   complex(mean(dataBBB(253:262,5:5)), length(dataBBB(253:262,5:5)));
                   complex(mean(dataBBB(263:343,5:5)), length(dataBBB(263:343,5:5)));
                   complex(mean(dataBBB(344:381,5:5)), length(dataBBB(344:381,5:5)));
                   complex(mean(dataBBB(382:449,5:5)), length(dataBBB(382:449,5:5)))];
  
 result667 = [complex(mean(dataBBB(1:46,6:6)), length(dataBBB(1:46,6:6)));
                   complex(mean(dataBBB(47:58,6:6)), length(dataBBB(47:58,6:6)));
                   complex(mean(dataBBB(59:123,6:6)), length(dataBBB(59:123,6:6)));
                   complex(mean(dataBBB(124:151,6:6)), length(dataBBB(124:151,6:6)));
                   complex(mean(dataBBB(152:252,6:6)), length(dataBBB(152:252,6:6)));
                   complex(mean(dataBBB(253:262,6:6)), length(dataBBB(253:262,6:6)));
                   complex(mean(dataBBB(263:329,6:6)), length(dataBBB(263:329,6:6)));
                   complex(mean(dataBBB(330:366,6:6)), length(dataBBB(330:366,6:6)));
                   complex(mean(dataBBB(367:449,6:6)), length(dataBBB(367:449,6:6)))];
               
 result777 = [complex(mean(dataBBB(1:46,7:7)), length(dataBBB(1:46,7:7)));
                   complex(mean(dataBBB(47:58,7:7)), length(dataBBB(47:58,7:7)));
                   complex(mean(dataBBB(59:122,7:7)), length(dataBBB(59:122,7:7)));
                   complex(mean(dataBBB(123:151,7:7)), length(dataBBB(123:151,7:7)));
                   complex(mean(dataBBB(152:252,7:7)), length(dataBBB(152:252,7:7)));
                   complex(mean(dataBBB(253:262,7:7)), length(dataBBB(253:262,7:7)));
                   complex(mean(dataBBB(263:343,7:7)), length(dataBBB(263:343,7:7)));
                   complex(mean(dataBBB(344:381,7:7)), length(dataBBB(344:381,7:7)));
                   complex(mean(dataBBB(382:449,7:7)), length(dataBBB(382:449,7:7)));];
               
               
       resultData3 = [result111 result222 result333 result444 result555 result667 result777];
    
    resultB41 = [complex(mean(dataB4(1:46,1:1)), length(dataB4(1:46,1:1)));
                   complex(mean(dataB4(47:58,1:1)), length(dataB4(47:58,1:1)));
                   complex(mean(dataB4(59:133,1:1)), length(dataB4(59:133,1:1)));
                   complex(mean(dataB4(134:162,1:1)), length(dataB4(134:162,1:1)));
                   complex(mean(dataB4(163:252,1:1)), length(dataB4(163:252,1:1)))];
               
    resultB42 = [complex(mean(dataB4(1:46,2:2)), length(dataB4(1:46,2:2)));
                   complex(mean(dataB4(47:58,2:2)), length(dataB4(47:58,2:2)));
                   complex(mean(dataB4(59:133,2:2)), length(dataB4(59:133,2:2)));
                   complex(mean(dataB4(134:162,2:2)), length(dataB4(134:162,2:2)));
                   complex(mean(dataB4(163:252,2:2)), length(dataB4(163:252,2:2)))];
               
    resultB43 = [complex(mean(dataB4(1:46,3:3)), length(dataB4(1:46,3:3)));
                   complex(mean(dataB4(47:58,3:3)), length(dataB4(47:58,3:3)));
                   complex(mean(dataB4(59:133,3:3)), length(dataB4(59:133,3:3)));
                   complex(mean(dataB4(134:162,3:3)), length(dataB4(134:162,3:3)));
                   complex(mean(dataB4(163:252,3:3)), length(dataB4(163:252,3:3)))];
               
     resultB44 = [complex(mean(dataB4(1:46,4:4)), length(dataB4(1:46,4:4)));
                   complex(mean(dataB4(47:58,4:4)), length(dataB4(47:58,4:4)));
                   complex(mean(dataB4(59:108,4:4)), length(dataB4(59:108,4:4)));
                   complex(mean(dataB4(109:139,4:4)), length(dataB4(109:139,4:4)));
                   complex(mean(dataB4(140:252,4:4)), length(dataB4(140:252,4:4)))];
               
    resultB45 = [complex(mean(dataB4(1:46,5:5)), length(dataB4(1:46,5:5)));
                   complex(mean(dataB4(47:58,5:5)), length(dataB4(47:58,5:5)));
                   complex(mean(dataB4(59:133,5:5)), length(dataB4(59:133,5:5)));
                   complex(mean(dataB4(134:162,5:5)), length(dataB4(134:162,5:5)));
                   complex(mean(dataB4(163:252,5:5)), length(dataB4(163:252,5:5)))];
               
    resultDataB4 = [resultB41 resultB42 resultB43 resultB44 resultB45];

end