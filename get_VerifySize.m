function [ result ] = get_VerifySize( data, winSize, n)
% Esse algoritmo faz o inverso do outro
% algoritmo em que verifica o tamanho.
% Ele converte o numero complexo da parte real
% para armazenar em uma matriz de colunas e linhas.
% Talvez ainda mude, por conta dos dados do sensorgrama.

result = zeros(length(data(:,1:1)), winSize);

for i = 1:1:winSize
   for j = 1:1:n         
       result(i,j) = real(data(i,j));
   end
end

result = result(:,1:1:end);

end

