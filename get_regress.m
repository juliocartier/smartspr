function [ beta_2, beta_1, value_estim, e, valor_IC_low, valor_IC_high, teste_hip, p_value_low, p_value_high] = get_regress(lambda, error) 

%Gerando os valores 1 e os coeficientes X
X = [ones(length(lambda),1) (1:length(lambda))'];

X1 = X'*X;
XY = X'*lambda;

%Valores dos coeficientes de beta1 e beta2
beta = inv(X1)*XY;

%Estimador de variância
y_trans = (lambda'*lambda - beta'*XY)/(length(lambda) - 2);

%Matriz de Covariância
Cjj = inv(X1);
%Valores dos coeficientes da regressão beta_1 é o valor beta 0
%E o valor beta_2 é o valor beta 1.
beta_1 = beta(1:1,1:1);
beta_2 = beta(2:2,1:1);
 
%Valor estimado da Regressão
value_estim = beta_1 + beta_2 * X(:, 2:2:2);
%m_a_line = sqrt(y_trans*Cjj(2:2,2:2));

e = lambda - value_estim;

%Teste de hipotese do coeficiente da regressão
teste_hip = (beta_2/sqrt(y_trans*Cjj(2:2,2:2)));
   
%P-valor de acordo com a tabela
 p_value_low = -tinv(1-error/2,length(lambda)-2);
 p_value_high = tinv(1-error/2,length(lambda)-2);
   
%Verificação do Teste de Hipotese com o p-value
teste_hipo = teste_hip > p_value_low & teste_hip < p_value_high;
  
%Intervalo de Confiança
valor_IC_low = -tinv(1-error/2,length(lambda)-2)*sqrt(y_trans*Cjj(2:2,2:2)) + beta_2;
valor_IC_high = tinv(1-error/2,length(lambda)-2)*sqrt(y_trans*Cjj(2:2,2:2)) + beta_2;
  
end

