function [ resultTotDescrt ] = get_dataClassificy( qtyCoefficiet )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% close all;
% clear all;
% clc;

load receitaBK7Ag.txt;

[data_group_b, data_group_c, data_group_d] = get_BK7AgGroup(receitaBK7Ag);

[result_group_A, result_group_AA, result_group_AAA, result_group_AAAA] = get_A(receitaBK7Ag);

[result_group_B, result_group_BB, result_group_BBB, result_group_BBBB] = get_B(data_group_b);

[result_group_C, result_group_CC, result_group_CCC] = get_C(data_group_c);

[result_group_D, result_group_DD, result_group_DDD, result_group_DDDD] = get_D(data_group_d);

%Descritor

[result_descrit_A, result_descrit_AA, result_descrit_AAA, result_descrit_AAAA] = get_descriA(result_group_A(:,1:7), result_group_AA(:,1:7), result_group_AAA(:,1:7), result_group_AAAA(:,1:5));

[result_descrit_B, result_descrit_BB, result_descrit_BBB, result_descrit_BBBB] = get_descriB(result_group_B(:,1:7), result_group_BB(:,1:7), result_group_BBB(:,1:7), result_group_BBBB(:,1:5));

[result_descrit_C, result_descrit_CC, result_descrit_CCC] = get_descriC(result_group_C(:,1:7), result_group_CC(:,1:7), result_group_CCC(:,1:7));

[result_descrit_D, result_descrit_DD, result_descrit_DDD, result_descrit_DDDD] = get_descriD(result_group_D(:,1:7), result_group_DD(:,1:7), result_group_DDD(:,1:7), result_group_DDDD(:,1:5));

winSize = qtyCoefficiet;

if (winSize <= 3)

    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    resultA4 = verificaTamanho(result_descrit_AAAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    resultB4 = verificaTamanho(result_descrit_BBBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    resultD4 = verificaTamanho(result_descrit_DDDD, winSize);

  resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultA4 repelem(4,length(resultA4(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))';
                     resultB3 repelem(7,length(resultB3(:,1:1)))'; resultB4 repelem(8,length(resultB4(:,1:1)))';
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))'; 
                     resultC3 repelem(11,length(resultC3(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))'; 
                     resultD2 repelem(13,length(resultD2(:,1:1)))'; resultD3 repelem(14,length(resultD3(:,1:1)))'; 
                     resultD4 repelem(15,length(resultD4(:,1:1)))'];
figure(4578)
plot3(resultTotDescrt(:,1:1), resultTotDescrt(:,2:2), resultTotDescrt(:,3:3), '.r', 'MarkerSize', 12);
  
elseif(winSize > 3 && winSize <= 5)
     
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    resultA3 = verificaTamanho(result_descrit_AAA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    resultB3 = verificaTamanho(result_descrit_BBB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    resultC3 = verificaTamanho(result_descrit_CCC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    resultD3 = verificaTamanho(result_descrit_DDD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultA3 repelem(3,length(resultA3(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))';
                     resultB2 repelem(6,length(resultB2(:,1:1)))'; resultB3 repelem(7,length(resultB3(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultC3 repelem(11,length(resultC3(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))';
                     resultD2 repelem(13,length(resultD2(:,1:1)))'; resultD3 repelem(14,length(resultD3(:,1:1)))'];

elseif(winSize > 5 && winSize <= 7)
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultA2 = verificaTamanho(result_descrit_AA, winSize);
    
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultB2 = verificaTamanho(result_descrit_BB, winSize);
    
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultC2 = verificaTamanho(result_descrit_CC, winSize);
    
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    resultD2 = verificaTamanho(result_descrit_DD, winSize);
    
resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultA2 repelem(2,length(resultA2(:,1:1)))';
                     resultB1 repelem(5,length(resultB1(:,1:1)))'; resultB2 repelem(6,length(resultB2(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultC2 repelem(10,length(resultC2(:,1:1)))';
                     resultD1 repelem(12,length(resultD1(:,1:1)))'; resultD2 repelem(13,length(resultD2(:,1:1)))'];
                 
else
    
    resultA1 = verificaTamanho(result_descrit_A, winSize);
    resultB1 = verificaTamanho(result_descrit_B, winSize);
    resultC1 = verificaTamanho(result_descrit_C, winSize);
    resultD1 = verificaTamanho(result_descrit_D, winSize);
    
 resultTotDescrt = [resultA1 repelem(1,length(resultA1(:,1:1)))'; resultB1 repelem(5,length(resultB1(:,1:1)))'; 
                     resultC1 repelem(9,length(resultC1(:,1:1)))'; resultD1 repelem(12,length(resultD1(:,1:1)))'];
    
            
                 
end


end

