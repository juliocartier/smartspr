function [ result ] = get_response( name, data )

if name == 'BK7Au'
    switch data
        case 1
            result = 'H_2O -> Hypo. -> H_2O -> PBS -> H_2O -> BSA -> H_2O -> Hypo. -> H_2O';
        case 2
            result = 'H_2O -> Hypo. -> H_2O -> PBS -> H_2O -> BSA -> H_2O';
    end

else
    switch data
        case 1
            result = 'H_2O -> Etanol 25% -> H_2O -> Etanol 12.5% -> H_2O -> BSA-monolayer -> H_2O ->  Etanol 25% -> H_2O';
        case 2
            result = 'H_2O -> Etanol 25% -> H_2O -> Etanol 12.5% -> H_2O -> BSA-monolayer -> H_2O';
        case 3
            result = 'H_2O -> Etanol 25% -> H_2O -> Etanol 12.5% -> H_2O';
        case 4
            result = 'H_2O -> Etanol 25% -> H_2O';
        case 5
            result = 'H_2O -> Etanol 12.5% -> H_2O -> Etanol 25% -> H_2O -> BSA-monolayer -> H_2O -> Etanol 25% -> H_2O';
        case 6
            result = 'H_2O -> Etanol 12.5% -> H_2O -> Etanol 25% -> H_2O -> BSA-monolayer -> H_2O';
        case 7
            result = 'H_2O -> Etanol 12.5% -> H_2O -> Etanol 25% -> H_2O';
        case 8
            result = 'H_2O -> Etanol 12.5% -> H_2O';
        case 9
            result = 'H_2O -> Etanol 12.5% -> H_2O -> BSA-Monolayer -> H_20 -> Etanol 25% -> H_2O -> Etanol 25% -> H_20';
        case 10
            result = 'H_2O -> Etanol 12.5% -> H_2O -> BSA-Monolayer -> H_20 -> Etanol 25% -> H_2O';
        case 11
            result = 'H_2O -> Etanol 12.5% -> H_2O -> BSA-Monolayer -> H_20';
        case 12
            result = 'H_2O -> BSA-Monolayer -> H_2O -> Etanol 25% -> H_2O -> Etanol 25% -> H_2O -> Etanol 12.5% -> H_2O';
        case 13
            result = 'H_2O -> BSA-Monolayer -> H_2O -> Etanol 25% -> H_2O -> Etanol 25% -> H_2O';
        case 14
            result = 'H_2O -> BSA-Monolayer -> H_2O -> Etanol 25% -> H_2O';
        case 15
            result = 'H_2O -> BSA-Monolayer -> H_2O';
    end
end


end

