function [ IC_A,  IC_B, lambdaRegress] = get_resultRegress( janela,  result)
%GET_RESULTREGRESS Summary of this function goes here
%   Detailed explanation goes here

lambdaRegress = [];
low_i = [];
high_i = [];
IC_A = [];
IC_B = [];
teste_h = [];
valor_estimado = [];

for i=1:1:length(result)
    ante = i-janela;
    pos = i+janela;
    if(ante <= 1)
        ante = 1;
    end
    
    if(pos > length(result))
       pos = length(result);
    end
    
    date = result(ante:pos); %anterior e posterior

    [lb_A, ~, valor_estim, ~, ic_coefA, ic_coefB, ~, ~, ~] = get_regress(date, 0.001);
   
    lambdaRegress = [lambdaRegress; lb_A];
    %valor_estimado = [valor_estimado; valor_estim];
    IC_A = [IC_A; ic_coefA];
    IC_B = [IC_B; ic_coefB];
    
  end


end

